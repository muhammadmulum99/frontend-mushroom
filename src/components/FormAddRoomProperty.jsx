import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js";


const FormAddRoomProperty = () => {
  const initialState = {
    id_properti_hotel: '',
    uuid_properti_kamar: '',
    type_kamar: '',
    fasilitas_kamar: '',
    rating: '',
    jumlah_kamar: '',
    harga_weekday: '',
    harga_weekend: '',
    foto_kamar: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertikamar", data);
      navigate("/roomproperty");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          foto_kamar: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }
  const [hotel, setHotel] = useState([]);
  const [kamar, setKamar] = useState([]);
  useEffect(() => {
    const getHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotel(decryptedData);
        }
        else{
          setHotel([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getKamar = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/masterkamar`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setKamar(decryptedData);
        }
        else{
          setKamar([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getHotel();
    getKamar();
  }, []);
  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">Add New Room</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">Hotel Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a registered hotel</option>
                    {hotel.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Room Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_properti_kamar"
                    value={data.uuid_properti_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a room type</option>
                    {kamar.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_master_kamar}>{item.type_kamar}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Type Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="type_kamar"
                    value={data.type_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Fasilitas Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="fasilitas_kamar"
                    value={data.fasilitas_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Rating</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="rating"
                    value={data.rating}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Jumlah Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="jumlah_kamar"
                    value={data.jumlah_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Harga Weekday</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="harga_weekday"
                    value={data.harga_weekday}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Harga Weekend</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="harga_weekend"
                    value={data.harga_weekend}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Foto Kamar</label>
                <div className="control">
                  <input
                    type="file"
                    className="input"
                    name="foto_kamar"
                    onChange={uploadFiles}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddRoomProperty;
