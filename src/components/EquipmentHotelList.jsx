import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Roomlist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/equipmenthotel");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/equipmenthotel/${userId}`);
    getUsers();
  };

  return (
    <div>
      <h1 className="title">Equipment</h1>
      <h2 className="subtitle">List of Equipment Hotel</h2>
      <Link to="/equipmenthotel/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Equipment Hotel</th>
            <th>ID Properti Facility Hotel</th>
            <th>Equipment Name</th>
            <th>Equipment Condition</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.uuid_equipment_hotel}</td>
              <td>{user.id_properti_fasilitas_hotel}</td>
              <td>{user.nama_equipment}</td>
              <td>{user.kondisi_equipment}</td>
              <td>
                <Link
                  to={`/equipmenthotel/edit/${user.id}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.id)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Roomlist;
