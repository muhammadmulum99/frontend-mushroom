import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormEditHotelProperty = () => {
  const initialState = {
    uuid_properti_hotel: '',
    jenis_hotel: '',
    alamat_hotel: '',
    provinsi_hotel: '',
    kecamatan_hotel: '',
    long_lat_hotel: '',
    id_owner: '',
    id_management: '',
    foto_hotel: '',
    fasilitas: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();
  const [manager, setManager] = useState([])
  const [owner, setOwner] = useState([])

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel/${id}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setData(decryptedData);
        }
        else{
          setData(initialState)
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getManager = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/usermanager`);
        // const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        // const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(response.data){
          setManager(response.data);
        }
        else{
          setManager([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getOwner = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/userowner`);
        // const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        // const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(response.data){
          setOwner(response.data);
        }
        else{
          setOwner([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getManager();
    getOwner();
    getUserById();
  }, [id]);

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/propertihotel/${data.id}`, data);
      navigate("/hotelproperty");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          foto_hotel: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  return (
    <div>
      <h1 className="title">Hotel Property</h1>
      <h2 className="subtitle">Update Hotel Property</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={updateUser}>
            <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Hotel Property</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_hotel"
                    value={data.uuid_properti_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Property Type</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="jenis_hotel"
                    value={data.jenis_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Address</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="alamat_hotel"
                    value={data.alamat_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Province</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="provinsi_hotel"
                    value={data.provinsi_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Kecamatan</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="kecamatan_hotel"
                    value={data.kecamatan_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Long Lat</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="long_lat_hotel"
                    value={data.long_lat_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">ID Owner</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_owner"
                    value={data.id_owner}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value=''>Pick an owner</option>
                    {owner.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid}>{item.user_name}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">ID Management</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_management"
                    value={data.id_management}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value=''>Pick a manager</option>
                    {manager.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid}>{item.user_name}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Foto Hotel</label>
                <div className="control">
                  <input
                    type="file"
                    className="input"
                    name="foto_hotel"
                    onChange={uploadFiles}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Fasilitas</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="fasilitas"
                    value={data.fasilitas}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEditHotelProperty;
