import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js"

const FormAddFacility = () => {
  const initialState = {
    // uuid_equipment_hotel: "",
    id_properti_hotel: '',
    id_properti_kamar: '',
    id_properti_fasilitas_hotel: "",
    nama_equipment: "",
    kondisi_equipment: "",
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/equipmenthotel", data);
      navigate("/equipmenthotel");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  const [hotel, setHotel] = useState([]);
  const [kamar, setKamar] = useState([]);
  const [fasilitas, setFasilitas] = useState([]);
  
  useEffect(() => {
    const getHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotel(decryptedData);
        }
        else{
          setHotel([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    
    getHotel();
  }, []);
  useEffect(() => {
    if(data.id_properti_hotel === '') return;
    const getFasilitas = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertifasilitashotelid/${data.id_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setFasilitas(decryptedData);
        }
        else{
          setFasilitas([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getFasilitas();
  },[data.id_properti_hotel])
  return (
    <div>
      <h1 className="title">Hotel</h1>
      <h2 className="subtitle">Update Hotel</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
            <p className="has-text-centered">{msg}</p>              
              {/* <div className="field">
                <label className="label">ID Equipment Hotel</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_equipment_hotel"
                    value={data.uuid_equipment_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>        */}
              <div className="field">
                <label className="label">Hotel Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a registered hotel</option>
                    {hotel.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Facility</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_properti_fasilitas_hotel"
                    value={data.id_properti_fasilitas_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Pick a facility</option>
                    {fasilitas.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_fasilitas_hotel}>{item.nama_master_fasilitas}</option>
                      )
                    })}
                  </select>
                </div>
              </div> 
              <div className="field">
                <label className="label">Equipment Name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_equipment"
                    value={data.nama_equipment}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>                    
              <div className="field">
                <label className="label">Equipment Condition</label>
                <div className="control">
                  <select
                    className="input"
                    name="kondisi_equipment"
                    value={data.kondisi_equipment}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Pick condition</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                    <option value="2">Under maintainance</option>
                  </select>
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddFacility;
