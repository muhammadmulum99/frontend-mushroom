import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormEditBank = () => {
  const initialState = {
    uuid_equipment_kamar: "",
    uuid_properti_hotel: '',
    uuid_properti_kamar: '',
    uuid_properti_kamar_detail: '',
    id_properti_fasilitas_kamar: "",
    nama_equipment: "",
    kondisi_equipment: "",
 }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();

  const [hotel, setHotel] = useState([]);
  const [kamar, setKamar] = useState([]);
  const [detil, setDetil] = useState([]);
  const [fasilitas, setFasilitas] = useState([]);

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/equipmentkamarall/${id}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setData(decryptedData);
        }
        else{
          setData(initialState)
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotel(decryptedData);
        }
        else{
          setHotel([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    
    getHotel();
    getUserById();
  }, [id]);

  useEffect(() => {
    if(data.uuid_properti_hotel === '') return;
    const getKamar = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertikamarid/${data.uuid_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setKamar(decryptedData);
        }
        else{
          setKamar([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getKamar();
  },[data.uuid_properti_hotel])

  useEffect(() => {
    if(data.uuid_properti_hotel === '' || data.uuid_properti_kamar === '') return;
    const getDetil = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertikamardetailid/${data.uuid_properti_hotel}/${data.uuid_properti_kamar}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setDetil(decryptedData);
        }
        else{
          setDetil([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getDetil();
  },[data.uuid_properti_hotel,data.uuid_properti_kamar])

  useEffect(() => {
    if(data.uuid_properti_hotel === '' || data.uuid_properti_kamar === '' || data.uuid_properti_kamar_detail === '') return;
    const getFasilitas = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertifasilitaskamardetailiddetail/${data.uuid_properti_hotel}/${data.uuid_properti_kamar}/${data.uuid_properti_kamar_detail}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setFasilitas(decryptedData);
        }
        else{
          setFasilitas([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getFasilitas();
  },[data.uuid_properti_hotel,data.uuid_properti_kamar,data.uuid_properti_kamar_detail])

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/equipmentkamar/${id}`, data);
      navigate("/equipmentroom");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Hotel</h1>
      <h2 className="subtitle">Update Hotel</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={updateUser}>
            <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">Hotel Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_properti_hotel"
                    value={data.uuid_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a registered hotel</option>
                    {hotel.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Room Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_properti_kamar"
                    value={data.uuid_properti_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a room type</option>
                    {kamar.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_kamar}>{item.type_kamar}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Room Number</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_properti_kamar_detail"
                    value={data.uuid_properti_kamar_detail}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a room type</option>
                    {detil.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_kamar_detail}>{item.nomor_kamar}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Facility</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_properti_fasilitas_kamar"
                    value={data.id_properti_fasilitas_kamar}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Pick a facility</option>
                    {fasilitas.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_fasilitas_kamar_detail}>{item.nama_master_fasilitas}</option>
                      )
                    })}
                  </select>
                </div>
              </div> 
              <div className="field">
                <label className="label">Equipment Name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_equipment"
                    value={data.nama_equipment}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>                    
              <div className="field">
                <label className="label">Equipment Condition</label>
                <div className="control">
                  <select
                    className="input"
                    name="kondisi_equipment"
                    value={data.kondisi_equipment}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Pick condition</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                    <option value="2">Under maintainance</option>
                  </select>
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEditBank;
