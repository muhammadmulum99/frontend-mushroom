import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddHotelCategory = () => {
  const initialState = {
    nama_master_kategori_hotel: '',
    bintang_master_kategori: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  // const [status, setStatus] = useState("");
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/masterkategorihotel", data);
      navigate("/hotelcategory");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Hotel Category</h1>
      <h2 className="subtitle">Add New Hotel Category</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              {/* <div className="field">
                <label className="label">ID Master Hotel Category</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_master_kategori_hotel"
                    value={data.uuid_master_kategori_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div> */}
              <div className="field">
                <label className="label">Nama Master Hotel Category</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_master_kategori_hotel"
                    value={data.nama_master_kategori_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Bintang Master Catergory</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="bintang_master_kategori"
                    value={data.bintang_master_kategori}
                    onChange={handleInputChange}
                    // placeholder="Email"
                  />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddHotelCategory;
