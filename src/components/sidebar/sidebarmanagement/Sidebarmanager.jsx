import React from "react";
import logo from "./logo.png";
import { Link } from "react-router-dom";

const Sidebarmanager = () => {
  return (
    <>
      <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
          <div class="navbar-brandmin header-logo">
            <a href="" class="b-brand">
              <img
                src={logo}
                alt=""
                style={{ height: "60px", width: "100%" }}
              />
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="javascript:">
              <span></span>
            </a>
          </div>
          <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
              <li class="nav-item pcoded-menu-caption">
                <label>Management Page</label>
              </li>
              <li
                data-username="form elements advance componant validation masking wizard picker select"
                class="nav-item"
              >
                <a href="/manager-profile" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-user"></i>
                  </span>
                  <span class="pcoded-mtext">Profile</span>
                  {/* <Link to="/owner-profile" style={{ color: "white" }}>
                    Products
                  </Link> */}
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-staff" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-server"></i>
                  </span>
                  <span class="pcoded-mtext">Staff</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-facilities" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-trending-up"></i>
                  </span>
                  <span class="pcoded-mtext">Facilities</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-rooms" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-file-text"></i>
                  </span>
                  <span class="pcoded-mtext">Rooms</span>
                </a>
              </li>

              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-financial" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-settings"></i>
                  </span>
                  <span class="pcoded-mtext">Financial</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-report" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-settings"></i>
                  </span>
                  <span class="pcoded-mtext">Report</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/manager-settings" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-settings"></i>
                  </span>
                  <span class="pcoded-mtext">Settings</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Sidebarmanager;
