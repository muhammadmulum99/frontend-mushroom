import React from "react";
import logo from "./logo.png";
const Sidebarfo = () => {
  return (
    <>
      <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
          <div class="navbar-brand header-logo">
            <a href="index.html" class="b-brand">
              <img
                src={logo}
                alt=""
                style={{ height: "60px", width: "100%" }}
              />
            </a>
            <a class="mobile-menu" id="mobile-collapse" href="javascript:">
              <span></span>
            </a>
          </div>
          <div class="navbar-content scroll-div">
            <ul class="nav pcoded-inner-navbar">
              <li class="nav-item pcoded-menu-caption">
                <label>Front Office</label>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/fo-checkin" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-file-text"></i>
                  </span>
                  <span class="pcoded-mtext">Checkin</span>
                </a>
              </li>
              <li
                data-username="form elements advance componant validation masking wizard picker select"
                class="nav-item"
              >
                <a href="/fo-checkout" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-user"></i>
                  </span>
                  <span class="pcoded-mtext">Checkout</span>
                  {/* <Link to="/owner-profile" style={{ color: "white" }}>
                Products
              </Link> */}
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                <a href="/fo-orderroom" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-server"></i>
                  </span>
                  <span class="pcoded-mtext">Order Room</span>
                </a>
              </li>
              <li
                data-username="Table bootstrap datatable footable"
                class="nav-item"
              >
                {/* <a href="/fo-roomchecker" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-trending-up"></i>
                  </span>
                  <span class="pcoded-mtext">Room Checker</span>
                </a> */}
                <a href="/fo-kasir" class="nav-link ">
                  <span class="pcoded-micon">
                    <i class="feather icon-trending-up"></i>
                  </span>
                  <span class="pcoded-mtext">Kasir</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Sidebarfo;
