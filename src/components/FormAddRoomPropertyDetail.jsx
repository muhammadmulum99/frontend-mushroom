import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormAddRoomPropertyDetail = () => {
  const initialState = {
    id_properti_hotel: '',
    id_properti_kamar: '',
    // uuid_properti_kamar_detail: '',
    nomor_kamar: '',
    lantai_kamar: '',
    status_kamar_detail: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertikamardetail", data);
      navigate("/roompropertydetail");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  const [hotel, setHotel] = useState([]);
  const [kamar, setKamar] = useState([]);
  useEffect(() => {
    const getHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotel(decryptedData);
        }
        else{
          setHotel([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getHotel();
  }, []);
  useEffect(() => {
    if(data.id_properti_hotel === '') return;
    const getKamar = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertikamarid/${data.id_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setKamar(decryptedData);
        }
        else{
          setKamar([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getKamar();
  },[data.id_properti_hotel])
  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">Add New Room</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">Hotel Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_properti_hotel"
                    value={data.id_properti_hotel}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a registered hotel</option>
                    {hotel.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Room Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_properti_kamar"
                    value={data.id_properti_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  >
                    <option value="">Pick a room type</option>
                    {kamar.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_master_kamar}>{item.type_kamar}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              {/* <div className="field">
                <label className="label">ID Properti Kamar Detail</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_properti_kamar_detail"
                    value={data.uuid_properti_kamar_detail}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div> */}
              <div className="field">
                <label className="label">Nomor Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nomor_kamar"
                    value={data.nomor_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Lantai Kamar</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="lantai_kamar"
                    value={data.lantai_kamar}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_kamar_detail"
                    value={data.status_kamar_detail}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>             
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddRoomPropertyDetail;
