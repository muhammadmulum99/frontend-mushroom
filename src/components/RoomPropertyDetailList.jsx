import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const RoomPropertyDetaillist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/propertikamardetail");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/propertikamardetail/${userId}`);
    getUsers();
  };

  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">List of Room</h2>
      <Link to="/roompropertydetail/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Hotel Property</th>
            <th>ID Room Property</th>
            <th>ID Room Property Detail</th>
            <th>Room Number</th>
            <th>Room Floor</th>
            <th>Status</th>

            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.id_properti_hotel}</td>
              <td>{user.id_properti_kamar}</td>
              <td>{user.uuid_properti_kamar_detail}</td>
              <td>{user.nomor_kamar}</td>
              <td>{user.lantai_kamar}</td>
              <td>{user.status_kamar_detail}</td>
              <td>
                <Link
                  to={`/roompropertydetail/edit/${user.uuid_properti_kamar_detail}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_properti_kamar_detail)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default RoomPropertyDetaillist;
