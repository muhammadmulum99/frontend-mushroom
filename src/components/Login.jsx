import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { LoginUser, reset } from "../features/authSlice";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = ("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { user, isError, isSuccess, isLoading, message } = useSelector(
    (state) => state.auth
  );

  useEffect(() => {
    if (user || isSuccess) {
      if(user.role === 'admin'){
        navigate("/dashboard");
      }
      else if(user.role === 'fo'){
        navigate("/fo-kasir");
      }
      else if(user.role === 'manager'){
        navigate("/manager-profile");
      }
      else if(user.role === 'owner'){
        navigate("/owner-profile");
      }
      else{
        navigate("/")
      }
    }
    dispatch(reset());
  }, [user, isSuccess, dispatch, navigate]);

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ email, password }));
  };

  return (
    <div class="auth-wrapper">
        <div class="auth-content">
          <div class="auth-bg">
            <span class="r"></span>
            <span class="r s"></span>
            <span class="r s"></span>
            <span class="r"></span>
          </div>
          <div class="card">
            <div class="card-body text-center">
              <div class="mb-4">
                <i class="feather icon-unlock auth-icon"></i>
              </div>
              <h3 class="mb-4">Login</h3>
              <div class="input-group mb-3">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Email or username"
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              <div class="input-group mb-4">
                <input
                  type="password"
                  class="form-control"
                  placeholder="password"
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              {isError && <p className="has-text-centered" style={{ color: 'red' }}>{message}</p>}
              <div class="form-group text-left">
                <div class="checkbox checkbox-fill d-inline">
                  <input
                    type="checkbox"
                    name="checkbox-fill-1"
                    id="checkbox-fill-a1"
                    checked=""
                  />
                  <label for="checkbox-fill-a1" class="cr">
                    {" "}
                    Save Details
                  </label>
                </div>
              </div>
              {/* {error && (
                <>
                  <small style={{ color: "red" }}>{error}</small>
                  <br />
                </>
              )} */}
              <br />
              {/* <button class="btn btn-primary shadow-2 mb-4">Login</button> */}
              <input
                type="button"
                class="btn btn-primary shadow-2 mb-4"
                disabled={isLoading ? true : false}
                value={isLoading ? "Loading..." : "Login"}
                onClick={Auth}
              />
              <br />
              <p class="mb-2 text-muted">
                Forgot password? <a href="auth-reset-password.html">Reset</a>
              </p>
              <p class="mb-0 text-muted">
                Don’t have an account? <a href="/register">Signup</a>
              </p>
            </div>
          </div>
        </div>
      </div>
  );
};

export default Login;
