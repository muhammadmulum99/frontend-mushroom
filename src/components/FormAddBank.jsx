import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const FormAddBank = () => {
  const initialState = {
    uuid_master_bank: '',
    nama_master_bank: '',
    status_master_bank: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/bank", data);
      navigate("/bank");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Bank</h1>
      <h2 className="subtitle">Add New Bank</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">Bank Name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_master_bank"
                    value={data.nama_master_bank}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status Bank</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_master_bank"
                    value={data.status_master_bank}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddBank;
