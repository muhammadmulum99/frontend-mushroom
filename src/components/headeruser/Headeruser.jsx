import React, { useState, useEffect } from "react";
import { Navbar, Container, Nav, NavDropdown, Button } from "react-bootstrap";
import logo from "./logo.png";
import { HiOutlineBuildingOffice2 } from "react-icons/hi2";
import { MdOutlineVerified } from "react-icons/md";
import { BsBasket3 } from "react-icons/bs";
import { getMe, LogOut } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Headeruser = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const [users, setUsers] = useState(false);

  const initialState = {
    id_user: ""
  }
  const [data, setData] = useState(initialState)

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      setUsers(false)
    }
    if(user){
      setUsers(true)
    }
  }, [isError, user, navigate]);

  const logout = () => {
    dispatch(LogOut());
    window.location.reload()
  };


  return (
    <>
      <Navbar bg="light" expand="lg" fixed="top">
        <Container>
          {/* {user.email} */}
          <Navbar.Brand href="/" className="test">
            <img
              src={logo}
              width="170px"
              height="56px"
              className="d-inline-block align-top"
              style={{ background: "light" }}
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto"></Nav>
            {users ? <Nav.Link href="/owner-properti" style={{ marginRight: "20px" }}>
              <HiOutlineBuildingOffice2
                style={{ marginRight: "5px", marginBottom: "5px" }}
              />{" "}
              List Your Properti
            </Nav.Link> : null}
            {users ? <Nav.Link href="/wishlist" style={{ marginRight: "20px" }}>
              <MdOutlineVerified
                style={{ marginRight: "0px", marginBottom: "5px" }}
              />{" "}
              Wishlist
            </Nav.Link>: null}
            {users? <Nav.Link href="/orders" style={{ marginRight: "20px" }}>
              <BsBasket3 style={{ marginRight: "5px", marginBottom: "5px" }} />
              My Orders
            </Nav.Link> : null}
            {/* <NavDropdown
              title="Language"
              id="basic-nav-dropdown"
              style={{ marginRight: "10px" }}
            >
              <NavDropdown.Item href="#action/3.1">Indonesia</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">English</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Jepang</NavDropdown.Item>
            </NavDropdown> */}
            <Button
              // href="/login"
              variant="warning"
              style={{
                right: "0",
                marginTop: "4px",
              }}
              onClick={() => users ? logout() : navigate('/login')}
            >
              {users? 'Logout' :'Login | Signup'}
            </Button>{" "}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default Headeruser;
