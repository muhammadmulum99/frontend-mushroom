import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Banklist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/propertifasilitashotel");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/propertifasilitashotel/${userId}`);
    getUsers();
  };

  const downloadBase64File = (base64Data, x) => {
    const linkSource = `data:image/jpeg;base64,${base64Data}`;
    const downloadLink = document.createElement("a");
    downloadLink.href = linkSource;
    downloadLink.download = x+'.jpeg';
    downloadLink.click();
  }

  return (
    <div>
      <h1 className="title">Property Facility Hotel</h1>
      <h2 className="subtitle">List of Property Facility Hotel</h2>
      <Link to="/facilitypropertyhotel/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Properti Fasilitas Hotel</th>
            <th>ID Properti Hotel</th>
            <th>ID Master Fasilitas</th>
            <th>Status</th>
            <th>Photo</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.uuid_properti_fasilitas_hotel}</td>
              <td>{user.id_properti_hotel}</td>
              <td>{user.id_master_fasilitas}</td>
              <td>{user.status_properti_fasilitas_hotel}</td>
              <td><button onClick={() => downloadBase64File(user.foto_fasilitas_hotel, user.id_master_fasilitas)}>Download Photo</button></td>
              <td>
                <Link
                  to={`/facilitypropertyhotel/edit/${user.uuid_properti_fasilitas_hotel}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_properti_fasilitas_hotel)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Banklist;
