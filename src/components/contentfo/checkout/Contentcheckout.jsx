import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { Row, Col } from "react-bootstrap";
import logo from "./benderaindo.jpg";
import "./contentcheckout.css";
import swal from "sweetalert";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js";

const Contentcheckin = () => {  
  let today = new Date();
  const navigate = useNavigate();
  
  const initialState = {
    id_reservasi: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    room_type: "",
    room_number: "",
    jumlah_kamar: "0",
    start_date: "",
    end_date: "",
    total_biaya: "",
    room_price: "",
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    tgl_transaksi: today.getFullYear()+'-'+
    (today.getMonth() > 8 + 1 ? today.getMonth() + 1 : '0'+today.getMonth() + 1)+'-'+
    (today.getDate() > 9 ? today.getDate() : '0'+today.getDate()),
    waktu_checkin: today.getFullYear()+'-'+
    (today.getMonth() > 8 + 1 ? today.getMonth() + 1 : '0'+today.getMonth() + 1)+'-'+
    (today.getDate() > 9 ? today.getDate() : '0'+today.getDate()) + ' ' + (today.getHours() > 9 ? today.getHours() : '0'+today.getHours())+':'+
    (today.getMinutes() > 9 ? today.getMinutes() : '0'+today.getMinutes())+':'+
    (today.getSeconds() > 9 ? today.getSeconds() : '0'+today.getSeconds()),
    id_properti_hotel: '',
    id_staff: '',
    id_properti_kamar: '',
    jenis_pembayaran: '',
    foto_kamar: '',
  }

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
    console.log("test");
  };

  const handlecheckin = () => {
    swal({
      title: "Apakah anda yakin?",
      text: "Pastikan Data yang di inputkan Sudah sesuai",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        axios({
          method: "post",
          url: "http://localhost:5000/checkin",
          data: {
            ...input,
            waktu_checkout: input.tgl_checkin + ' ' + (today.getHours() > 9 ? today.getHours() : '0'+today.getHours())+':'+
            (today.getMinutes() > 9 ? today.getMinutes() : '0'+today.getMinutes())+':'+
            (today.getSeconds() > 9 ? today.getSeconds() : '0'+today.getSeconds()),       
          }
        })
          .then(function (response) {
            axios({
              method: "patch",
              url: "http://localhost:5000/historicheckin/"+input.id_reservasi,
              data: {
                ...input,
                waktu_checkout: input.tgl_checkin + ' ' + (today.getHours() > 9 ? today.getHours() : '0'+today.getHours())+':'+
                (today.getMinutes() > 9 ? today.getMinutes() : '0'+today.getMinutes())+':'+
                (today.getSeconds() > 9 ? today.getSeconds() : '0'+today.getSeconds()),       
              }
            })
              .then(function (response) {
                swal(response.data.msg, {
                  icon: "success",
                });
              })
              .catch(err => {
                if (err.message && err.message.includes('413')) {
                  swal(err.response.statusText, {
                    icon: "error",
                  });
                  // console.log(err)
                }
              });
          })
          .catch(err => {
            if (err.message && err.message.includes('413')) {
              swal(err.response.statusText, {
                icon: "error",
              });
              // console.log(err)
            }
          });
        
      } else {
        swal("Terimakasih sudah memastikan");
      }
    });
  };

  const getBooking = () => {
    axios({
      method: "get",
      url: "http://localhost:5000/getreservasi/"+input.id_reservasi,
    })
      .then(function (response) {
        if(response.data){
          const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
          const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
          setInput({
            ...input,     
            id_transaksi: decryptedData[0].id_transaksi,       
            name: decryptedData[0].name,
            // dob: response.data[0].dob,
            email: decryptedData[0].email,
            phone_number: decryptedData[0].phone_number,
            nik_passport: decryptedData[0].nik_passport,
            address: decryptedData[0].address,            
            // room_name: response.data[0].room_name,
            room_type: decryptedData[0].type_kamar,
            jumlah_kamar: today.getDay() > 4 ? parseInt(decryptedData[0].total_biaya)/parseInt(decryptedData[0].harga_weekend) : parseInt(decryptedData[0].total_biaya)/parseInt(decryptedData[0].harga_weekday),
            total_biaya: decryptedData[0].total_biaya,
            room_price: today.getDay() > 4 ? decryptedData[0].harga_weekend : decryptedData[0].harga_weekday,
            tgl_checkin: decryptedData[0].tgl_checkin,
            tgl_checkout: decryptedData[0].tgl_checkout,
            id_properti_kamar: decryptedData[0].uuid_properti_kamar,
            id_properti_hotel: decryptedData[0].uuid_properti_hotel,
            jenis_pembayaran: decryptedData[0].jenis_pembayaran,
            foto_kamar: decryptedData[0].foto_kamar,
          })
        }
      })
      .catch(err => {
        console.log(err)
        // if (err.message && err.message.includes('413')) {
        //   swal(err.response.statusText, {
        //     icon: "error",
        //   });
        //   // console.log(err)
        // }
      });
  }

  useEffect(() => {
    if(input.id_reservasi === "") return;
    getBooking()
  },[input.id_reservasi])
  

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if(user){
      setInput({...input, id_user: user.uuid, name: user.user_name})
    }
  }, [isError, user, navigate]);

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/staff/${input.name}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if(decryptedData){
      setInput({
        ...input,
        id_properti_hotel: decryptedData.id_properti_hotel,
        id_staff: decryptedData.uuid_staff,
      })
    }
  };

  useEffect(() => {
    if(input.name === '') return;
    getStaff();
  },[input.name])

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Front Office</h5>
                      </div>
                      <ul class="breadcrumb">
                        {/* <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li> */}
                        <li class="breadcrumb-item">
                          <a href="/fo-checkin">Checkin</a>
                        </li>
                        {/* <li class="breadcrumb-item">
                          <a href="javascript:"></a>
                        </li> */}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-body">
                          <Row>
                            <Col>
                              {" "}
                              <h5>Check in</h5>
                            </Col>
                          </Row>
                        </div>
                        {/* awal */}
                        <div class="card-body">
                          <Form>
                            <Form.Group
                              className="mb-3"
                              controlId="formBasicEmail"
                            >
                              <Form.Label>Customer ID</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter customer ID"
                                name="id_reservasi"
                                onChange={handleInputChange}
                                style={{ marginBottom: "10px" }}
                              />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicPassword"
                            >
                              <Form.Label>Name</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Name"
                                value={input.name}
                              />
                            </Form.Group>
                            {/* <Form.Group
                              className="mb-3"
                              controlId="formBasicEmail"
                            >
                              <Form.Label>Date of Birth</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Date of Birth"
                                value={input.dob}
                              />
                            </Form.Group> */}

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicPassword"
                            >
                              <Form.Label>Email</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Email"
                                value={input.email}
                              />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicEmail"
                            >
                              <Form.Label>Phone Number</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Phone number"
                                value={input.phone_number}
                              />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicPassword"
                            >
                              <Form.Label>
                                NIK/Passport Number
                              </Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter NIK/Passport Number"
                                value={input.nik_passport}
                              />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicEmail"
                            >
                              <Form.Label>Address</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Address"
                                value={input.address}
                              />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formBasicPassword"
                            >
                              <Form.Label>Special Needs</Form.Label>
                              <Form.Control
                                type="text"
                                placeholder="Enter Special Needs"
                                // value={input.special_need}
                              />
                            </Form.Group>
                          </Form>
                        </div>
                        {/* akhir */}
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Details Room</h5>
                        </div>
                        <div class="card-body">
                          <div className="main-contentkiriform">
                            <div className="contentkiri-fotonama">
                              {/* <img src={logo} alt="" /> */}
                              {input.foto_kamar === '' ? null : <img src={`data:image/jpeg;base64,${input.foto_kamar}`}  alt="kamar" />}
                              <div className="hotelnama">
                                <h3>{input.room_type}</h3>
                                <h3>x {input.jumlah_kamar}</h3>
                              </div>
                            </div>
                            <div className="contentkiri-border">
                              <div className="contentkiri-checkin">
                                <div className="checkin">
                                  <h3>Check In</h3>
                                  <p>{input.tgl_checkin}</p>
                                </div>
                                <div className="checkout">
                                  <h3>Check Out</h3>
                                  <p>{input.tgl_checkout}</p>
                                </div>
                              </div>
                              <hr align="left" width="550" />
                              {/* <div className="contentkiri-room">
                                <div className="contentkiri-roomtype">
                                  <h3>Special Need</h3>
                                  <p>{input.special_need}</p>
                                </div>
                              </div> */}
                            </div>
                          </div>
                        </div>
                        <Button
                          variant="outline-warning"
                          style={{
                            marginLeft: "10px",
                            marginRight: "10px",
                          }}
                          onClick={handlecheckin}
                        >
                          Checkin
                        </Button>{" "}
                      </div>
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Contentcheckin;
