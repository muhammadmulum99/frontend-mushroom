import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col, Modal, Table } from "react-bootstrap";
import swal from "sweetalert";
import { getMe } from "../../../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js";
import axios from "axios";

const Formedit = () => {
  let today = new Date();
  const navigate = useNavigate();
  const [modal, setModal] = useState(false)
  const [kamar, setKamar] = useState([])

  const handleClose = () => setModal(false);

  const handleOrder = () => {
    localStorage.setItem('fo', JSON.stringify(input))
    navigate('fo-payment')
  };

  const initialState = {
    id_reservasi: "",
    name: "",
    dob: "",
    email: "",
    jenis_kelamin: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    room_type: "",
    room_number: "",
    jumlah_kamar: "0",
    start_date: "",
    end_date: "",
    total_biaya: "",
    room_price: "",
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    tgl_transaksi: today.getFullYear()+'-'+
    (today.getMonth() > 9 ? today.getMonth() + 1 : '0'+(today.getMonth()+1))+'-'+
    (today.getDate() > 9 ? today.getDate() + 1 : '0'+(today.getDate())),
    waktu_checkin: today.getFullYear()+'-'+
    (today.getMonth() > 8 + 1 ? today.getMonth() + 1 : '0'+(today.getMonth() + 1))+'-'+
    (today.getDate() > 9 ? today.getDate() : '0'+today.getDate()) + ' ' + (today.getHours() > 9 ? today.getHours() : '0'+today.getHours())+':'+
    (today.getMinutes() > 9 ? today.getMinutes() : '0'+today.getMinutes())+':'+
    (today.getSeconds() > 9 ? today.getSeconds() : '0'+today.getSeconds()),
    id_properti_hotel: '',
    id_staff: '',
    id_properti_kamar: '',
    jenis_pembayaran: '',
    foto_kamar: '',
    bukti_pembayaran: 'xxx',
    email_property: ''
  }

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
    console.log("test");
  };

  // const handlekirimwa = () => {
  //   alert("test wa");
  // };

  // const handlekirimemail = () => {
  //   alert("test email");
  // };

  const getBooking = () => {
    axios({
      method: "get",
      url: "http://localhost:5000/getreservasi/"+input.id_reservasi,
    })
      .then(function (response) {
        if(response.data){
          const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
          const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
          if(decryptedData){
            setInput({
              ...input,     
              id_transaksi: decryptedData[0].id_transaksi,       
              name: decryptedData[0].name,
              // dob: response.data[0].dob,
              jenis_kelamin: decryptedData[0].jenis_kelamin,
              email: decryptedData[0].email,
              phone_number: decryptedData[0].phone_number,
              nik_passport: decryptedData[0].nik_passport,
              address: decryptedData[0].address,            
              // room_name: response.data[0].room_name,
              room_type: decryptedData[0].type_kamar,
              jumlah_kamar: today.getDay() > 4 ? parseInt(decryptedData[0].total_biaya)/parseInt(decryptedData[0].harga_weekend) : parseInt(decryptedData[0].total_biaya)/parseInt(decryptedData[0].harga_weekday),
              total_biaya: decryptedData[0].total_biaya,
              room_price: today.getDay() > 4 ? decryptedData[0].harga_weekend : decryptedData[0].harga_weekday,
              tgl_checkin: decryptedData[0].tgl_checkin,
              tgl_checkout: decryptedData[0].tgl_checkout,
              id_properti_kamar: decryptedData[0].uuid_properti_kamar,
              id_properti_hotel: decryptedData[0].uuid_properti_hotel,
              jenis_pembayaran: decryptedData[0].jenis_pembayaran,
              foto_kamar: decryptedData[0].foto_kamar,
            })
          }
        }
      })
      .catch(err => {
        console.log(err)
        // if (err.message && err.message.includes('413')) {
        //   swal(err.response.statusText, {
        //     icon: "error",
        //   });
        //   // console.log(err)
        // }
      });
  }

  useEffect(() => {
    if(input.id_reservasi === "") return;
    getBooking()
  },[input.id_reservasi])

  const handleSisaKamar = () => {
    axios({
      method: "get",
      url: "http://localhost:5000/getsisakamar/" + input.id_properti_hotel+'/'+ input.tgl_checkin+'/'+ input.tgl_checkout,
    })
      .then(function (response) {
        if(response.data){          
          const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
          const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
          // console.log(decryptedData)
          setKamar(decryptedData)
          setModal(true);
        }
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  }

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel/${input.id_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        console.log(decryptedData)
        if(decryptedData){
          setInput({
            ...input,
            email_property: decryptedData.email_hotel
          });
        }
      } catch (error) {
        if (error.response) {
          alert(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [input.id_properti_hotel]);

  useEffect(() => {
    if(input.tgl_checkout === ''){ 
      return;
    }
    else if(new Date(input.tgl_checkout) <= new Date(input.tgl_checkin)){
      swal('Tanggal Checkout tidak bisa lebih kecil dari Tanggal Checkin', {
        icon: "error",
      }).then((success) => {
        if(success){
          setInput({
            ...input,
            tgl_checkin: '',
            tgl_checkout: '',
          })
        }
      })
      ;
    }
    else{
      handleSisaKamar();
    }
  },[input.tgl_checkout])

  useEffect(() => {
    if(input.jumlah_kamar === '0' || ''){ 
      return;
    }
    else if(parseInt(input.jumlah_kamar) > parseInt(input.jumlah_temp)){
      swal('Tidak sesuai sisa kamar', {
        icon: "error",
      }).then((success) => {
        if(success){
          setInput({
            ...input,
            jumlah_kamar: 0,
          })
        }
      })
      ;
    }
  },[input.jumlah_kamar])
  

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if(user){
      setInput({...input, id_user: user.uuid, name: user.user_name})
    }
  }, [isError, user, navigate]);

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/staff/${input.name}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if(decryptedData){
      setInput({
        ...input,
        id_properti_hotel: decryptedData.id_properti_hotel,
        id_staff: decryptedData.uuid_staff,
      })
    }
  };

  useEffect(() => {
    if(input.name === '') return;
    getStaff();
  },[input.name])

  
  return (
    <div>
      {/* <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Control
          type="text"
          placeholder="Enter customer ID"
          name="booking_id"
          onChange={handleInputChange}
          style={{ marginBottom: "10px" }}
        />
      </Form.Group> */}
      <div class="card-body">
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Customer ID</Form.Label>
            <Form.Control type="text" placeholder="Enter customer ID" name="id_reservasi" onChange={handleInputChange} />
          </Form.Group> 
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="Full Name" name="name" value={input.name} onChange={handleInputChange} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Date of Birth</Form.Label>
            <Form.Control type="date" placeholder="" name="dob" value={input.dob} onChange={handleInputChange}/>
          </Form.Group>
          <Form.Group
            className="mb-3"
            controlId="formBasicPassword"
          >
            <Form.Label>Gender</Form.Label>
            <Form.Control
              as="select"
              name="jenis_kelamin"
              value={input.jenis_kelamin}
              onChange={handleInputChange}
            >
              <option value="">Select your gender</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Enter email" name="email" value={input.email} onChange={handleInputChange} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control type="text" placeholder="Phone Number" name="phone_number" value={input.phone_number} onChange={handleInputChange}/>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>NIK/Passport Number</Form.Label>
            <Form.Control type="text" placeholder="NIK/Passport Number" name="nik_passport" value={input.nik_passport} onChange={handleInputChange}/>
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Address</Form.Label>
            <Form.Control type="text" placeholder="Enter Address" name="address" value={input.address} onChange={handleInputChange} />
          </Form.Group>
          <Row>
            <Col>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Check In</Form.Label>
                <Form.Control type="date" placeholder="" name="tgl_checkin" value={input.tgl_checkin} onChange={handleInputChange}/>
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Check out</Form.Label>
                <Form.Control type="date" placeholder="" name="tgl_checkout" value={input.tgl_checkout} onChange={handleInputChange}/>
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Room Type</Form.Label>
                <Form.Control type="text" placeholder="Enter Address" name="room_type" value={input.room_type} onChange={handleInputChange}/>
              </Form.Group>
            </Col>
            <Col>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Rooms</Form.Label>
                <Form.Control type="number" placeholder="Enter Rooms" name="jumlah_kamar" value={input.jumlah_kamar} onChange={(e) => {
                  setInput({
                    ...input,
                    jumlah_kamar: e.target.value,
                    total_biaya: (parseInt(input.room_price) * parseInt(e.target.value) * (new Date(input.tgl_checkout).getDate() - new Date(input.tgl_checkin).getDate())).toString()
                  });
                }} />
              </Form.Group>
            </Col>
          </Row>
          {/* <Button
            variant="outline-warning"
            style={{
              float: "right",
            }}
            onClick={handlekirimwa}
          >
            Bukti Wa
          </Button>{" "}
          <Button
            variant="outline-warning"
            style={{
              float: "right",
            }}
            onClick={handlekirimemail}
          >
            Bukti Email
          </Button>{" "} */}
          <Button
            variant="outline-warning"
            style={{
              float: "right",
              height: "40px",
              width: "100px",
              marginTop: "20px",
            }}
            href="/fo-payment"
            onClick={() => {localStorage.setItem("fo", JSON.stringify(input));}}
          >
            Pay
          </Button>{" "}
        </Form>
        <Modal show={modal} onHide={handleClose} size='lg'>
          <Modal.Header closeButton>
            <Modal.Title>Cari Kamar</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Table striped>
              <thead>
                <tr>                  
                  <th>Tipe Kamar</th>
                  <th>Harga</th>
                  <th>Sisa</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {kamar.map((item, key) => {
                  return(
                    <tr key={key}>
                      <th>{item.type_kamar}</th>
                      <th>{new Date(input.tgl_checkin).getDay() > 4 ? item.harga_weekend : item.harga_weekday}</th>
                      <th>{item.get_sisa_kamar}</th>
                      <th><Button onClick={() => {
                        handleClose()
                        setInput({
                          ...input,
                          room_type: item.type_kamar,
                          id_properti_kamar: item.uuid_properti_kamar,
                          jumlah_temp: item.get_sisa_kamar,
                          room_price: new Date(input.tgl_checkin).getDay() > 4 ? item.harga_weekend : item.harga_weekday,
                        })
                      }}>Select</Button></th>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>        
      </div>
    </div>
  );
};

export default Formedit;
