import React from "react";
import Calanderhotel from "./fullcalnder/Calanderhotel";
import Formorder from "./formorderdanedit/Formorder";
import Formedit from "./formorderdanedit/Formedit";
const Contentorderroom = () => {
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    {/* <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Callender Hotel</h5>
                        </div>
                        <div class="card-body">
                          <Calanderhotel />
                        </div>
                      </div>
                    </div> */}

                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Order Room</h5>
                        </div>
                        <div class="card-body">
                          <Formorder />
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Edit Order</h5>
                        </div>
                        <div class="card-body">
                          <Formedit />
                        </div>
                      </div>
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Contentorderroom;
