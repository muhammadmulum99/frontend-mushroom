import React from "react";
import { Button, Table, Dropdown, Image, Card } from "react-bootstrap";
import florplan from "./florplan.jpg";

const Contentchecker = () => {
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Hotel Floor Plan</h5>
                          <div style={{ float: "right" }}>
                            <Dropdown>
                              <Dropdown.Toggle
                                variant="light"
                                id="dropdown-basic"
                              >
                                Floor
                              </Dropdown.Toggle>

                              <Dropdown.Menu>
                                <Dropdown.Item href="#/action-1">
                                  Lantai 1
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-2">
                                  Laintai 2
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-3">
                                  Lantai 3
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                        <div class="card-body">
                          <Card.Img variant="top" src={florplan} />
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Details</h5>
                        </div>
                        <div class="card-body">
                          <h6>Floor Details</h6>
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Facilities</th>
                                <th>Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>Lobby</td>
                                <td>Open</td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Restaurant</td>
                                <td>Closed</td>
                              </tr>
                              <tr>
                                <td>3</td>
                                <td>Fitness Center</td>
                                <td>Open</td>
                              </tr>
                              <tr>
                                <td>4</td>
                                <td>Swimmingpool</td>
                                <td>Maintenance</td>
                              </tr>
                              <tr>
                                <td>5</td>
                                <td>Laundry</td>
                                <td>Closed</td>
                              </tr>
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Contentchecker;
