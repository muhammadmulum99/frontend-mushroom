import React, { useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import swal from "sweetalert";
const Formkasir = (props) => {

  const handleOrder = () => {
    console.log(props)
    // swal({
    //   title: "Are you sure?",
    //   text: "Pastikan Data yang di inputkan Sudah sesuai",
    //   icon: "warning",
    //   buttons: true,
    //   dangerMode: true,
    // }).then((willDelete) => {
    //   if (willDelete) {
    //     swal("Success Order! enjoy :)", {
    //       icon: "success",
    //     });
    //   } else {
    //     swal("Terimakasih sudah memastikan");
    //   }
    // });
  };

  const initialState = {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    room_type: "",
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: "",
    room_price: "",
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: props.temp.tgl_checkin,
    tgl_checkout: props.temp.tgl_checkout,
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
  }

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
    console.log("test");
  };

  const handlekirimwa = () => {
    alert("test wa");
  };

  const handlekirimemail = () => {
    alert("test email");
  };

  // const transaksi = [
  //   {
  //     booking_id: "1",
  //     name: "Jokowow",
  //     dob: "10/09/1999",
  //     email: "wakanda@mail.com",
  //     phone_number: "-",
  //     nik_passport: "3287382",
  //     address: "wakanda",
  //     special_need: "testt testt",
  //     room_name: "kamar raja",
  //     room_type: "king room",
  //     room_number: "kamar 04",
  //     start_date: "2022-09-15",
  //     end_date: "2022-09-17",
  //     total_biaya: "120.000",
  //     room_price: "100.000",
  //     additions: "10.000",
  //     platfrom_fee: "10.000",
  //     ppn: "free",
  //   },
  //   {
  //     booking_id: "2",
  //     name: "Puan chan",
  //     dob: "18/03/2000",
  //     email: "wakanda@mail.com",
  //     phone_number: "-",
  //     nik_passport: "8217817",
  //     address: "wakanda",
  //     special_need: "testt testt",
  //     room_name: "kamar raja",
  //     room_type: "king room",
  //     room_number: "kamar 04",
  //     start_date: "2022-09-15",
  //     end_date: "2022-09-17",
  //     total_biaya: "120.000",
  //     room_price: "100.000",
  //     additions: "10.000",
  //     platfrom_fee: "10.000",
  //     ppn: "free",
  //   },
  // ];

  // const [data, setData] = useState([
  //   {
  //     booking_id: "1",
  //     room_type: "Type A",
  //     room_facilities: "gym",
  //     room_availability: "belum penuh",
  //     room_price: "5,000,000",
  //     rooms_ordered: "1",
  //     start_date: "2022-09-15",
  //     end_date: "2022-09-17",
  //     room_number: "",
  //   },
  //   {
  //     booking_id: "1",
  //     room_type: "Type B",
  //     room_facilities: "gym",
  //     room_availability: "belum penuh",
  //     room_price: "5,000,000",
  //     rooms_ordered: "1",
  //     start_date: "2022-09-15",
  //     end_date: "2022-09-17",
  //     room_number: "",
  //   },
  // ]);
  return (
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Reference Number</Form.Label>
        <Form.Control type="text" placeholder="Reference Number" name="no_ref" onChange={handleInputChange} />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Source of Booking</Form.Label>
        <Form.Control type="text" placeholder="Source of Booking" name="sumber" onChange={handleInputChange}  />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Full Name" name="name" onChange={handleInputChange}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword"  >
        <Form.Label>Date of Birth</Form.Label>
        <Form.Control type="date" placeholder="" name="dob" onChange={handleInputChange}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="Enter email" name="email" onChange={handleInputChange}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Phone Number</Form.Label>
        <Form.Control type="text" placeholder="Phone Number" name="no_hp" onChange={handleInputChange}/>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>NIK/Passport Number</Form.Label>
        <Form.Control type="text" placeholder="NIK/Passport Number" name="nik_passport" onChange={handleInputChange} />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Address</Form.Label>
        <Form.Control type="text" placeholder="Enter Address" name="address" onChange={handleInputChange} />
      </Form.Group>
      <Row>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Check In</Form.Label>
            <Form.Control value={input.tgl_checkin} type="date" placeholder="" name="tgl_checkin" onChange={handleInputChange} />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Check out</Form.Label>
            <Form.Control value={input.tgl_checkout} type="date" placeholder="" name="tgl_checkout" onChange={handleInputChange} />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Room Type</Form.Label>
            <Form.Control type="text" placeholder="Enter Room Type" name="room_type" onChange={handleInputChange} />
          </Form.Group>
        </Col>
        <Col>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Rooms</Form.Label>
            <Form.Control type="text" placeholder="Enter Rooms" name="jumlah_kamar" onChange={handleInputChange} />
          </Form.Group>
        </Col>
      </Row>
      {/* <Form.Group className="mb-3">
        <Form.Label>Disabled select menu</Form.Label>
        <Form.Select>
          <option>Cash</option>
          <option>Scan QR</option>
          <option>BCA</option>
        </Form.Select>
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Upload File</Form.Label>
        <Form.Control type="file" placeholder="" />
      </Form.Group> */}
      <Button
        variant="outline-warning"
        style={{
          float: "right",
          height: "40px",
          width: "100px",
          marginTop: "20px",
        }}
        onClick={handleOrder}
        // href="/fo-payment"
        // onClick={() => {sessionStorage.setItem("riwayat", input);}}
      >
        Pay
      </Button>{" "}
    </Form>
  );
};

export default Formkasir;
