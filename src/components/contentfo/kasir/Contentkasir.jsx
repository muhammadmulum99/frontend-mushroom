import React, { useState } from "react";
// import Calanderhotel from "./fullcalnder/Calanderhotel";
// import Calanderhotel from "../roomorder/fullcalnder/Calanderhotel";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import { Form, Button, Row, Col, Modal, Table } from "react-bootstrap";
import axios from "axios";
import { useEffect } from "react";
import swal from "sweetalert";
import cryptoJs from "crypto-js";
import { getMe } from "../../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
const Contentkasir = () => {
  const navigate = useNavigate();
  const [count, setCount] = useState(1)
  const [show, setShow] = useState(true)
  const [modal, setModal] = useState(false)
  const [kamar, setKamar] = useState([])
  const today = new Date();
  // const handleOrder = () => {
  //   swal({
  //     title: "Are you sure?",
  //     text: "Pastikan Data yang di inputkan Sudah sesuai",
  //     icon: "warning",
  //     buttons: true,
  //     dangerMode: true,
  //   }).then((willDelete) => {
  //     if (willDelete) {
  //       swal("Success Order! enjoy :)", {
  //         icon: "success",
  //       });
  //     } else {
  //       swal("Terimakasih sudah memastikan");
  //     }
  //   });
  // };

  const initialState = {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    room_type: "",
    room_number: "",
    jumlah_kamar: "0",
    jumlah_temp: "0",
    start_date: "",
    end_date: "",
    total_biaya: "",
    room_price: "",
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    tgl_transaksi: today.getFullYear()+'-'+
    (today.getMonth() > 9 ? today.getMonth() + 1 : '0'+(today.getMonth()+1))+'-'+
    (today.getDate() > 9 ? today.getDate() + 1 : '0'+(today.getDate())),
    id_properti_hotel: '',
    id_properti_kamar: '',
    jenis_pembayaran: '',
    bukti_pembayaran: '',
    id_staff: '',
    email_property: ''
  }

  const [input, setInput] = useState(initialState);

  const handleClose = () => setModal(false);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
    // console.log("test");
  };

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel/${input.id_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        console.log(decryptedData)
        if(decryptedData){
          setInput({
            ...input,
            email_property: decryptedData.email_hotel
          });
        }
      } catch (error) {
        if (error.response) {
          alert(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [input.id_properti_hotel]);

  const handleSisaKamar = () => {
    axios({
      method: "get",
      url: "http://localhost:5000/getsisakamar/" + input.id_properti_hotel+'/'+ input.tgl_checkin+'/'+ input.tgl_checkout,
    })
      .then(function (response) {
        if(response.data){          
          const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
          const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
          // console.log(decryptedData)
          setKamar(decryptedData)
          setModal(true);
        }
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  }

  useEffect(() => {
    if(input.tgl_checkout === ''){ 
      return;
    }
    else if(new Date(input.tgl_checkout) <= new Date(input.tgl_checkin)){
      swal('Tanggal Checkout tidak bisa lebih kecil dari Tanggal Checkin', {
        icon: "error",
      }).then((success) => {
        if(success){
          setInput({
            ...input,
            tgl_checkin: '',
            tgl_checkout: '',
          })
        }
      })
      ;
    }
    else{
      handleSisaKamar();
    }
  },[input.tgl_checkout])

  useEffect(() => {
    if(input.jumlah_kamar === '0' || ''){ 
      return;
    }
    else if(parseInt(input.jumlah_kamar) > parseInt(input.jumlah_temp)){
      swal('Tidak sesuai sisa kamar', {
        icon: "error",
      }).then((success) => {
        if(success){
          setInput({
            ...input,
            jumlah_kamar: 0,
          })
        }
      })
      ;
    }
  },[input.jumlah_kamar])

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if(user){
      setInput({...input, id_user: user.uuid, username: user.user_name})
    }
  }, [isError, user, navigate]);

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/staff/${input.username}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if(decryptedData){
      setInput({
        ...input,
        id_properti_hotel: decryptedData.id_properti_hotel,
        id_staff: decryptedData.uuid_staff,
      })
    }
  };

  useEffect(() => {
    if(input.username === '') return;
    getStaff();
  },[input.username])

  const handleOrder = () => {
    localStorage.setItem('fo', JSON.stringify(input))
    navigate("/fo-payment")
  };

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    {/* <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Callender Hotel</h5>
                        </div>
                        <div class="card-body">
                        {show ? <FullCalendar
                          plugins={[dayGridPlugin, interactionPlugin]}
                          initialView="dayGridMonth"
                          dateClick={function (info) {
                            if(count === 1){
                              console.log(info)
                              info.dayEl.style.backgroundColor = 'red';
                              setCount(2)
                              setInput({
                                ...input,
                                tgl_checkin: info.dateStr
                              })
                            }
                            else{
                              console.log(info)
                              info.dayEl.style.backgroundColor = 'blue';
                              setCount(1)
                              setInput({
                                ...input,
                                tgl_checkout: info.dateStr
                              })                              
                              setShow(false)
                            }
                          }}
                          events={[
                            { title: "Penuh", date: "2022-11-01", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-04", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-07", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-15", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-16", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-17", backgroundColor: "red" },
                            { title: "Penuh", date: "2022-11-20", backgroundColor: "red" },
                          ]}
                        /> : null}
                        </div>
                      </div>
                    </div> */}

                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header">
                          <h5>Order Room Kasir</h5>
                        </div>
                        <div class="card-body">
                          <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Reference Number</Form.Label>
                              <Form.Control type="text" placeholder="Reference Number" name="no_ref" onChange={handleInputChange} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Source of Booking</Form.Label>
                              <Form.Control type="text" placeholder="Source of Booking" name="sumber" onChange={handleInputChange}  />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Name</Form.Label>
                              <Form.Control type="text" placeholder="Full Name" name="name" onChange={handleInputChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword"  >
                              <Form.Label>Date of Birth</Form.Label>
                              <Form.Control type="date" placeholder="" name="dob" onChange={handleInputChange}/>
                            </Form.Group>
                            <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                              <Form.Label>Gender</Form.Label>
                              <Form.Control
                                as="select"
                                name="jenis_kelamin"
                                onChange={handleInputChange}
                              >
                                <option value="">Select your gender</option>
                                <option>Male</option>
                                <option>Female</option>
                              </Form.Control>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Email</Form.Label>
                              <Form.Control type="email" placeholder="Enter email" name="email" onChange={handleInputChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Phone Number</Form.Label>
                              <Form.Control type="text" placeholder="Phone Number" name="phone_number" onChange={handleInputChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>NIK/Passport Number</Form.Label>
                              <Form.Control type="text" placeholder="NIK/Passport Number" name="nik_passport" onChange={handleInputChange} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                              <Form.Label>Address</Form.Label>
                              <Form.Control type="text" placeholder="Enter Address" name="address" onChange={handleInputChange} />
                            </Form.Group>
                            <Row>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                  <Form.Label>Check In</Form.Label>
                                  <Form.Control value={input.tgl_checkin} type="date" placeholder="" name="tgl_checkin" onChange={handleInputChange} />
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                  <Form.Label>Check out</Form.Label>
                                  <Form.Control value={input.tgl_checkout} type="date" placeholder="" name="tgl_checkout" onChange={handleInputChange} />
                                </Form.Group>
                              </Col>
                            </Row>
                            <Row>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                  <Form.Label>Room Type</Form.Label>
                                  <Form.Control value={input.room_type} type="text" placeholder="Enter Room Type" name="room_type" onChange={handleInputChange} />
                                </Form.Group>
                              </Col>
                              <Col>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                  <Form.Label>Rooms</Form.Label>
                                  <Form.Control type="number" placeholder="Enter Rooms" name="jumlah_kamar" value={input.jumlah_kamar} onChange={(e) => {
                                    setInput({
                                      ...input,
                                      jumlah_kamar: e.target.value,
                                      total_biaya: (parseInt(input.room_price) * parseInt(e.target.value) * (new Date(input.tgl_checkout).getDate() - new Date(input.tgl_checkin).getDate())).toString()
                                    });
                                  }} />
                                </Form.Group>
                              </Col>
                            </Row>
                            {/* <Form.Group className="mb-3">
                              <Form.Label>Disabled select menu</Form.Label>
                              <Form.Select>
                                <option>Cash</option>
                                <option>Scan QR</option>
                                <option>BCA</option>
                              </Form.Select>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                              <Form.Label>Upload File</Form.Label>
                              <Form.Control type="file" placeholder="" />
                            </Form.Group> */}
                            <Button
                              variant="outline-warning"
                              style={{
                                float: "right",
                                height: "40px",
                                width: "100px",
                                marginTop: "20px",
                              }}
                              href='/fo-payment' 
                              onClick={() => localStorage.setItem('fo', JSON.stringify(input))}                            
                            >
                              Pay
                            </Button>{" "}
                          </Form>
                        </div>
                      </div>
                    </div>
                    <div className="col md-6">
                       {modal ? 
                        <Table striped>
                      <thead>
                        <tr>                  
                          <th>Tipe Kamar</th>
                          <th>Harga</th>
                          <th>Sisa</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {kamar.map((item, key) => {
                          return(
                            <tr key={key}>
                              <th>{item.type_kamar}</th>
                              <th>{new Date(input.tgl_checkin).getDay() > 4 ? item.harga_weekend : item.harga_weekday}</th>
                              <th>{item.get_sisa_kamar}</th>
                              <th><Button onClick={() => {
                                handleClose()
                                setInput({
                                  ...input,
                                  room_type: item.type_kamar,
                                  id_properti_kamar: item.uuid_properti_kamar,
                                  jumlah_temp: item.get_sisa_kamar,
                                  room_price: new Date(input.tgl_checkin).getDay() > 4 ? item.harga_weekend : item.harga_weekday,
                                })
                              }}>Select</Button></th>
                            </tr>
                          )
                        })}
                      </tbody>
                    </Table> : null}       
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <Modal show={modal} onHide={handleClose} size='lg'>
          <Modal.Header closeButton>
            <Modal.Title>Cari Kamar</Modal.Title>
          </Modal.Header>
          <Modal.Body>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal> */}
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Contentkasir;
