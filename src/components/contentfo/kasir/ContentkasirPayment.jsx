import React, { useEffect, useState } from "react";
import axios from "axios";
import { Form, Row, Col, Button, Card } from "react-bootstrap";
import swal from "sweetalert";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import { getMe } from "../../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";
import emailjs from "@emailjs/browser";

const ContentkasirPayment = () => {
  const navigate = useNavigate();
  const initialState = localStorage.getItem("fo") ? JSON.parse(localStorage.getItem("fo")) : {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    room_type: "",
    room_number: "",
    jumlah_kamar: "0",
    start_date: "",
    end_date: "",
    total_biaya: "",
    room_price: "",
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: '',
    tgl_checkout: '',
    no_ref: "",
    sumber: "",
    tgl_transaksi: '',
    id_properti_hotel: '',
    id_properti_kamar: '',
    code_promo: '',
    bukti_pembayaran: '',
    email_property: '',
  }
  
  const [riwayat, setRiwayat] = useState(initialState)

  const handleriwayatChange = (event) => {
    setRiwayat({
      ...riwayat,
      [event.target.name]: event.target.value,
    });
  };

  const [pem, setPem] = useState([])

  let empty = ''

  const handlePem = () => {
    axios({
      method: "get",
      url: "http://localhost:5000/masterpembayaran",
    })
      .then(function (response) {
        if(response.data){
          const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
          const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
          setPem(decryptedData)
          empty = '1'
        }
      })
      .catch(function (response) {
        //handle error
        console.log(response);
      });
  }

  const handleSimpan = () => {    
    swal({
      title: "Apakah anda yakin?",
      text: "Pastikan Data yang di inputkan Sudah sesuai",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        axios({
          method: "post",
          url: "http://localhost:5000/transaksi",
          data: riwayat
        })
          .then(function (response) {
            swal('Data berhasil di simpan', {
              icon: "success",
            }).then((success) => {
              if(success){
                downloadPdfDocument();
                emailjs.send("service_92jz21f","template_lkrz283",{
                  email: riwayat.email,
                  name: riwayat.name,
                  jenis_hotel: riwayat.jenis_hotel,
                  id_transaksi: response.data.msg,
                  tgl_transaksi: riwayat.tgl_transaksi,
                  room_type: riwayat.room_type,
                  room_price: riwayat.room_price,
                  jumlah_kamar: riwayat.jumlah_kamar,
                  total_biaya: riwayat.total_biaya,
                  email_property: riwayat.email_property,        
                  status: riwayat.jenis_pembayaran === 'COD' ? 'MENUNGGU PEMBAYARAN' : 'LUNAS'
                },"F0FZPc_8tzDxXO61Q")
                .then(
                  (result) => {
                    console.log(result.text);
                  },
                  (error) => {
                    console.log(error.text);
                  }
                );
              }
            });
          })
          .catch(err => {
            if (err.message && err.message.includes('413')) {
              swal(err.response.statusText, {
                icon: "error",
              });
              // console.log(err)
            }
          });
        
      } else {
        swal("Terimakasih sudah memastikan");
      }
    });
    
  }
  

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setRiwayat({
          ...riwayat,
          bukti_pembayaran: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  
  useEffect(() => {
    if(empty !== '') return;
    handlePem();
  },[empty])

  const downloadPdfDocument = () => {
    const input = document.getElementById("testId");
    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF();
            pdf.addImage(imgData, 'JPEG', 0, 0);
            pdf.save("struktest.pdf");
        })
  }

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if(user){
      setRiwayat({...riwayat, id_user: user.uuid, username: user.user_name})
    }
  }, [isError, user, navigate]);

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/staff/${riwayat.username}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if(decryptedData){
      setRiwayat({
        ...riwayat,
        id_properti_hotel: decryptedData.id_properti_hotel,
        id_staff: decryptedData.uuid_staff,
      })
    }
  };

  useEffect(() => {
    if(riwayat.username === '') return;
    getStaff();
  },[riwayat.username])

  return (
    <div class="pcoded-main-container">
      <div class="pcoded-wrapper">
        <div class="pcoded-content">
          <div class="pcoded-inner-content">
            {/* [ breadcrumb ] start */}
            <div class="page-header">
              <div class="page-block">
                <div class="row align-items-center">
                  <div class="col-md-12">
                    <div class="page-header-title">
                      <h5 class="m-b-10">Typography</h5>
                    </div>
                    <ul class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="index.html">
                          <i class="feather icon-home"></i>
                        </a>
                      </li>
                      <li class="breadcrumb-item">
                        <a href="javascript:">Basic Componants</a>
                      </li>
                      <li class="breadcrumb-item">
                        <a href="javascript:">Typography</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            {/* [ breadcrumb ] end */}
            <div class="main-body">
              <div class="page-wrapper">
                {/* [ Main Content ] start */}
                <div class="row">
                  {/* [ Typography ] start */}
                  <div class="col-sm-6">
                    <div class="card">
                      <div class="card-header">
                        <h5>Callender Hotel</h5>
                      </div>
                      <div class="card-body">
                        <Form>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicEmail"
                          >
                            <Row>
                              <Form.Label>Reference Number</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.no_ref}</Form.Label>
                            </Row>                            
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicEmail"
                          >
                            <Row>
                              <Form.Label>Source of Booking</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.sumber}</Form.Label>
                            </Row>                                                         
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicEmail"
                          >
                            <Row>
                              <Form.Label>Name</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.name}</Form.Label>
                            </Row>                                                            
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Row>
                              <Form.Label>Date of Birth</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.dob}</Form.Label>
                            </Row>                                                           
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicEmail"
                          >
                            <Row>
                              <Form.Label>Email</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.email}</Form.Label>
                            </Row>                                                         
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Row>
                              <Form.Label>Phone Number</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.phone_number}</Form.Label>
                            </Row>                                                           
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicEmail"
                          >
                            <Row>
                              <Form.Label>NIK/Passport Number</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.nik_passport}</Form.Label>
                            </Row>                                                              
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Row>
                              <Form.Label>Address</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.address}</Form.Label>
                            </Row> 
                          </Form.Group>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Row>
                              <Form.Label>Special Need</Form.Label>
                            </Row>
                            <Row>
                              <Form.Label>{riwayat.special_need}</Form.Label>
                            </Row> 
                          </Form.Group>
                          {/* <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Form.Label>Code Promo</Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Code Promo"
                              onChange={handleriwayatChange}
                            />
                          </Form.Group> */}
                          <Row>
                            <Col>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Row>
                                  <Form.Label>Check In</Form.Label>
                                </Row>
                                <Row>
                                  <Form.Label>{riwayat.tgl_checkin}</Form.Label>
                                </Row>                                                                 
                              </Form.Group>
                            </Col>
                            <Col>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Row>
                                  <Form.Label>Check out</Form.Label>
                                </Row>
                                <Row>
                                  <Form.Label>{riwayat.tgl_checkout}</Form.Label>
                                </Row>                                                                
                              </Form.Group>
                            </Col>
                          </Row>
                          <Row>
                            <Col>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Row>
                                  <Form.Label>Room Type</Form.Label>
                                </Row>
                                <Row>
                                  <Form.Label>{riwayat.room_type}</Form.Label>
                                </Row> 
                              </Form.Group>
                            </Col>
                            <Col>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Row>
                                  <Form.Label>Rooms</Form.Label>
                                </Row>
                                <Row>
                                  <Form.Label>{riwayat.jumlah_kamar}</Form.Label>
                                </Row> 
                              </Form.Group>
                            </Col>
                            <Row>
                              <Form.Group
                                className="mb-3"
                                controlId="formBasicPassword"
                              >
                                <Form.Label>Method of Payment</Form.Label>
                                <Form.Control
                                  as="select"
                                  name="jenis_pembayaran"
                                  onChange={handleriwayatChange}
                                >
                                  <option value="">Select method of payment</option>
                                  <option>COD</option>
                                   {pem.map((item,key) => {
                                      return(
                                        <option>{item.nama_master_pembayaran}</option>
                                      )
                                   })}
                                </Form.Control>
                              </Form.Group>
                            </Row>
                          </Row>
                          {/* <Button
    variant="outline-warning"
    style={{
      float: "right",
    }}
    onClick={handlekirimwa}
  >
    Bukti Wa
  </Button>{" "}
  <Button
    variant="outline-warning"
    style={{
      float: "right",
    }}
    onClick={handlekirimemail}
  >
    Bukti Email
  </Button>{" "} */}
                        </Form>
                      </div>
                    </div>
                  </div>

                  <div id="testId" class="col-md-6">
                    <div class="card">
                      <div class="card-header">
                        <h5>Order Room Kasir</h5>
                      </div>
                      <div class="card-body">
                        <div className="main-contentkiriform">
                          <div className="contentkiri-roomprice">
                            <h3>Name</h3>
                            <p>{riwayat.name}</p>
                          </div>
                          <div className="contentkiri-ppn">
                            <h3>Email</h3>
                            <p>{riwayat.email}</p>
                          </div>
                          <div className="contentkiri-fee">
                            <h3>Phone Number</h3>
                            <p>{riwayat.phone_number}</p>
                          </div>
                          <div className="contentkiri-promo">
                            <h3>Duration of Stay</h3>
                            <p>{new Date(riwayat.tgl_checkout).getDate() - new Date(riwayat.tgl_checkin).getDate()} Night {new Date(riwayat.tgl_checkout).getDate() - new Date(riwayat.tgl_checkin).getDate()} Day</p>
                          </div>
                          <div className="contentkiri-refund">
                            <h3>Room</h3>
                            <p>{riwayat.jumlah_kamar}x {riwayat.room_type} Rp.{riwayat.room_price}</p>
                          </div>
                          {/* <div className="contentkiri-refund">
                            <h3>Addition</h3>
                            <p>Ektra Bed Rp.123</p>
                          </div> */}
                          <div className="contentkiri-refund">
                            <h3>Promo</h3>
                            <p>Not Availble</p>
                          </div>
                          <div className="contentkiri-refund">
                            <h3>Payment Method</h3>
                            <p>{riwayat.jenis_pembayaran}</p>
                          </div>
                          <div className="contentkiri-refund">
                            <h3>Admin Fee</h3>
                            <p>Rp. 1000</p>
                          </div>
                          <hr align="left" width="554" />
                          <div className="contentkiri-price">
                            <h3>Total Price</h3>
                            <p>Rp.{Number(parseFloat(riwayat.total_biaya).toFixed(2).toString()).toLocaleString()}</p>
                          </div>                          
                        </div>
                      </div>
                    </div>
                    <div>
                      <Row>
                        <Col>
                          <Form.Group
                            className="mb-3"
                            controlId="formBasicPassword"
                          >
                            <Form.Label>Upload verification</Form.Label>
                            <Form.Control
                              type="file"
                              placeholder="Enter Address"
                              onChange={uploadFiles}
                            />
                          </Form.Group>
                          <Button
                            onClick={handleSimpan}
                            variant="outline-warning"
                            style={{
                              float: "right",
                            }}
                            // onClick={handleOrder}
                          >
                            Order Now
                          </Button>{" "}
                        </Col>
                      </Row>
                    </div>
                  </div>

                  {/* [ Typography ] end */}
                </div>
                {/* [ Main Content ] end */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentkasirPayment;
