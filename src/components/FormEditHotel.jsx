import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormEditBank = () => {
  const initialState = {
    uuid_master_hotel: '',
    id_master_kategori_hotel: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [hotel, setHotel] = useState([]);
  const [category, setCategory] = useState([]);
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/masterhotel/${id}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setData(decryptedData);
        }
        else{
          setData(initialState)
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotel(decryptedData);
        }
        else{
          setHotel([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    const getCategoryHotel = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/masterkategorihotel`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setCategory(decryptedData);
        }
        else{
          setCategory([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getUserById();
    getHotel();
    getCategoryHotel();
  }, [id]);

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/masterhotel/${id}`, data);
      navigate("/hotel");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Hotel</h1>
      <h2 className="subtitle">Update Hotel</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={updateUser}>
            <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Master Hotel</label>
                <div className="control">
                  <select
                    className="input"
                    name="uuid_master_hotel"
                    value={data.uuid_master_hotel}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Pick a registered hotel</option>
                    {hotel.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">ID Master Catergory Hotel</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_master_kategori_hotel"
                    value={data.id_master_kategori_hotel}
                    onChange={handleInputChange}
                    // placeholder="Email"
                  >
                    <option value="">Pick a category</option>
                    {category.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_master_kategori_hotel}>{item.nama_master_kategori_hotel}</option>
                      )
                    })}
                  </select>
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEditBank;
