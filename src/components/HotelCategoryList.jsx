import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Banklist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/masterkategorihotel");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    console.log(decryptedData)
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/masterkategorihotel/${userId}`);
    getUsers();
  };

  return (
    <div>
      <h1 className="title">Hotel Category</h1>
      <h2 className="subtitle">List of Hotel Category</h2>
      <Link to="/hotelcategory/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Master Category Hotel</th>
            <th>Nama Master Category Hotel</th>
            <th>Bintang Category Hotel</th>

            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid_master_kategori_hotel}>
              <td>{index + 1}</td>
              <td>{user.uuid_master_kategori_hotel}</td>
              <td>{user.nama_master_kategori_hotel}</td>
              <td>{user.bintang_master_kategori}</td>
              <td>
                <Link
                  to={`/hotelcategory/edit/${user.uuid_master_kategori_hotel}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_master_kategori_hotel)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Banklist;
