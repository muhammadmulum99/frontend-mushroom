import React from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contenthoteluser.css";
import SearchItem from "./Searchitem";
import axios from "axios";
import cryptoJs from "crypto-js";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";

const Contenthoteluser = () => {
  const [hotels, setHotels] = useState([])
  const [fasilitas, setFasilitas] = useState("");
  const [minimum, setMinimum] = useState("");
  const [maximum, setMaximum] = useState("");
  let today = new Date();
  let col = today.getDay() > 4 ? 'harga_weekdend' : 'harga_weekday'
  const { lokasi, checkin, checkout } = useParams()

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/searchhotel/${lokasi}/${checkin}/${checkout}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setHotels(decryptedData);
        }
        else{
          setHotels([])
        }
      } catch (error) {
        if (error.response) {
          alert(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [lokasi,checkin,checkout]);

  const getFilter = async () => {
    try {
      const params ={
        lokasi: lokasi,
        tgl_checkin: checkin,
        tgl_checkout: checkout,
        col: col,
        minimum: minimum,
        maximum: maximum,
        fasilitas: fasilitas.replace(/,/g, '')
      }
      const response = await axios.get('http://localhost:5000/filterhotel',{params});
      const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
      const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
      if(decryptedData){
        setHotels(decryptedData);
      }
      else{
        setHotels([])
      }
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };
  
  // let hotels = localStorage.getItem('hotels') ? JSON.parse(localStorage.getItem('hotels')) : []
  function valueLabelFormat(value) {
    const units = ["rupiah"];

    let unitIndex = 0;
    let scaledValue = value;

    while (scaledValue >= 50000 && unitIndex < units.length - 1) {
      unitIndex += 1;
      scaledValue *= 10;
    }

    return `${scaledValue} ${units[unitIndex]}`;
  }

  function calculateValue(value) {
    return value;
  }

  const [value, setValue] = React.useState(10);

  const handleChange = (event, newValue) => {
    if (typeof newValue === "number") {
      setValue(newValue);
    }
  };

  useEffect(() => {
    
  })

  return (
    <>
      <div className="main-content-hotels">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={6} md={4} style={{}}>
              <Card>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Budget</Form.Label>
                      <Form.Control
                        type='number'
                        placeholder='Min'
                        onChange={(e) => setMinimum(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Control
                        type='number'
                        placeholder='Max'
                        onChange={(e) => setMaximum(e.target.value)}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Payment</Form.Label>
                      <Form.Check
                        aria-label="option 1"
                        label="Payment on Check in"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'1',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'1',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Down-Payment"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'2',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'2',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>
                    <Form.Label>Facility</Form.Label>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Breakfast"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'3',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'3',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Gym"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'4',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'4',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Swiming Pool"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'5',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'5',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Bar"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'6',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'6',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Water Heater"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'7',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'7',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Park"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'8',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'8',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Check
                        aria-label="option 1"
                        label="Sports"
                        name="group1"
                        onChange={(e) => {
                          if(e.target.checked){
                            setFasilitas(fasilitas+"'9',")
                          }
                          else{
                            setFasilitas(fasilitas.replace(/'9',/g, ''))
                          }                        
                        }}
                      />
                    </Form.Group>
                    <Button variant="warning" onClick={getFilter} size="lg">
                      Submit
                    </Button>
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={12} md={8} style={{}}>
              <Card>
                <Card.Body>
                  {hotels.map((item,key) => {
                    return(
                      <SearchItem item={item} checkin={checkin} checkout={checkout}/>    
                    )
                  })}
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contenthoteluser;
