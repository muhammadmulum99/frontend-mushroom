import { useNavigate } from "react-router-dom";
import "./searchitem.css";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";
import { getMe, LoginUser } from "../../features/authSlice";
const SearchItem = ({ item, checkin, checkout }) => {
  let today = new Date();
  const [modal, setModal] = useState(false);
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const handledetail = () => {
    navigate("/hotel/"+item.uuid_properti_hotel+"/"+checkin+"/"+checkout);
    localStorage.setItem('detail',JSON.stringify(data))
  };
  const initialState = {
    ...item,
    tgl_checkin: '',
    tgl_checkout: '',
    total_biaya: '',
    jumlah_kamar: '',
    id_user: "",
    id_properti_hotel: item.uuid_properti_hotel
  }
  const [show, setShow] = useState(false)
  const [data, setData] = useState(initialState)
  
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }

  // const handleHarga = (e) => {
  //   let date = new Date(data.tgl_checkin)
  //   if(date.getDay() > 4){
  //     setData({
  //       ...data,
  //       total_biaya: (parseInt(item.harga_weekend) * parseInt(data.jumlah_kamar)).toString()
  //     })
  //   }
  //   else{
  //     setData({
  //       ...data,
  //       total_biaya: (parseInt(item.harga_weekday) * parseInt(data.jumlah_kamar)).toString()
  //     })
  //   }
  // }

  // useEffect(() => {
  //   if(data.jumlah_kamar === '') return;
  //   handleHarga()
  // }, [data.jumlah_kamar]);

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_user: user.uuid, username: user.user_name})
    }
  }, [isError, user, navigate]);

  const handleWishlist = async () => {
    try {
      await axios.post("http://localhost:5000/keranjang", data);
      navigate("/wishlist");
    } catch (error) {
      if (error.response) {
        // alert(error.response.data.msg);
        setModal(true)
      }
    }
  }

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ email, password }));
    // window.location.reload()
    setModal(false)
  };

  return (
    <>
      <div className="searchItem">
        <img
          src={item.foto_hotel ? "data:image/jpeg;base64," + item.foto_hotel: "https://cf.bstatic.com/xdata/images/hotel/square600/261707778.webp?k=fa6b6128468ec15e81f7d076b6f2473fa3a80c255582f155cae35f9edbffdd78&o=&s=1"}
          alt=""
          className="siImg"
        />
        <div className="siDesc">
          <h1 className="siTitle">{item.jenis_hotel}</h1>
          <span className="siDistance">500m from center</span>
          <span className="siTaxiOp">Diskon 20%</span>
          {/* <span className="siSubtitle">
            {item.type_kamar}
          </span> */}
          <span className="siFeatures">
            {item.fasilitas}
          </span>
          <span className="siCancelOp">Free cancellation </span>
          <span className="siCancelOpSubtitle">
            You can cancel later, so lock in this great price today!
          </span>
        </div>
        <div className="siDetails">
          <div className="siRating">
            <span>Excellent</span>
            <button>{item.rating}</button>
          </div>
          <div className="siDetailTexts">
            <span className="siPrice">Rp.{today.getDay() > 4 ? item.harga_weekend : item.harga_weekday}</span>
            <span className="siTaxOp">Includes taxes and fees</span>
            <button className="siCheckButton" onClick={handledetail}>
              See availability
            </button>
            {/* <button className="siCheckButton" onClick={() => setShow(true)}>
              Wishlist
            </button> */}
          </div>
        </div>
        {show ?
          <div className="siDesc">
            <input name="tgl_checkin" type="date" onChange={handleInputChange}/>
            <input name="tgl_checkout" type="date" onChange={handleInputChange}/>
            {/* <input name="jumlah_kamar" type="number" onChange={handleInputChange}/> */}
            <button className="siCheckButton" onClick={handleWishlist}>
              Add to Wishlist
            </button>
          </div> : null    
        }
      </div>
      <Modal show={modal} onHide={() => setModal(false)}>
      <Modal.Body>
        <div class="card">
          <div class="card-body text-center">
            <div class="mb-4">
              <i class="feather icon-unlock auth-icon"></i>
            </div>
            <h3 class="mb-4">Login</h3>
            <div class="input-group mb-3">
              <input
                type="text"
                class="form-control"
                placeholder="Email or username"
                onChange={(e) => setEmail(e.target.value)}
                autoComplete="new-password"
              />
            </div>
            <div class="input-group mb-4">
              <input
                type="password"
                class="form-control"
                placeholder="password"
                onChange={(e) => setPassword(e.target.value)}
                autoComplete="new-password"
              />
            </div>
            <div class="form-group text-left">
              <div class="checkbox checkbox-fill d-inline">
                <input
                  type="checkbox"
                  name="checkbox-fill-1"
                  id="checkbox-fill-a1"
                  checked=""
                />
                <label for="checkbox-fill-a1" class="cr">
                  {" "}
                  Save Details
                </label>
              </div>
            </div>
            {/* {error && (
              <>
                <small style={{ color: "red" }}>{error}</small>
                <br />
              </>
            )} */}
            <br />
            {/* <button class="btn btn-primary shadow-2 mb-4">Login</button> */}
            <input
              type="button"
              class="btn btn-primary shadow-2 mb-4"
              value='Login'
              onClick={Auth}
            />
            <br />
            <p class="mb-2 text-muted">
              Forgot password? <a href="auth-reset-password.html">Reset</a>
            </p>
            <p class="mb-0 text-muted">
              Don’t have an account? <a href="/register">Signup</a>
            </p>
          </div>
        </div>
      </Modal.Body>
    </Modal>
    </>
  );
};

export default SearchItem;
