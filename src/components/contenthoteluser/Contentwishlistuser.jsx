import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contenthoteluser.css";
import SearchItem from "./Wishlistitem";
import axios from "axios";
import cryptoJs from "crypto-js";
import { getMe } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Contentwishlistuser = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);

  const initialState = {
    id_user: ""
  }
  const [data, setData] = useState(initialState)

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      navigate("/");
    }
    if(user){
      setData({...data, id_user: user.uuid})
    }
  }, [isError, user, navigate]);

  useEffect(() => {
    if(data.id_user === '') return;
    getUsers();
  }, [data.id_user]);

  const getUsers = async () => {
    const response = await axios.get(`http://localhost:5000/keranjang/${data.id_user}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/keranjang/${userId}`);
    getUsers();
  };

  return (
    <>
      <div className="main-content-hotels">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}          
          {users.map((item,key) => {
            return(
              <Row>
                <Col xs={12} md={8} style={{}}>
                  <SearchItem item={item} />
                </Col>
                <Col>
                  <button
                    onClick={() => deleteUser(item.uuid_keranjang)}
                    className="button is-small is-danger"
                  >
                    Delete
                  </button>
                </Col>
              </Row> 
            )
          })}

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contentwishlistuser;
