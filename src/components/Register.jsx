// import React from "react";
import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import validator from "validator";
import { useEffect } from "react";

function Register(props) {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const initialState = {
    first_name: '',
    last_name: '',
    dob: '',
    gender: '',
    address: '',
    email: '',
    name: '',
    password: '',
    confPassword: '',
    nik_passport: '',
    phone_number: '',
    role: 'user',
  }
  const [data, setData] = useState(initialState);
  const [error, setError] = useState(null);

  const [errorMessage, setErrorMessage] = useState('')

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
 
  const validate = (e) => {
 
    if (validator.isStrongPassword(e, {
      minLength: 8, minLowercase: 1,
      minUppercase: 1, minNumbers: 1, minSymbols: 1
    })) {
      setErrorMessage('Is Strong Password')
      setLoading(false)
    } else {
      setErrorMessage('Is Not Strong Password')
      setLoading(true)
    }
  }

  useEffect(() => {
    if(data.password === '') return;
    validate(data.password)
  },[data.password])

  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios({
      method: "post",
      url: "http://localhost:5000/users",
      data: data
    })
      .then(function (response) {
        // setFact(JSON.stringify(response.data.data.email));
        // setLoading(false);
        // setUserSession('', response.data);
        // if(response.data){
        //   if(response.data[0].role === 'fo'){
        //     navigate("/fo-kasir");
        //   }
        //   else if(response.data[0].role === 'sa'){
        //     navigate("/superadmin-home");
        //   }
        // }
        alert(response.data.msg)
        setLoading(false)
        setData(initialState)
        //handle success
        // console.log(response.data);
      })
      .catch(function (response) {
        //handle error
        alert(response.response.data.msg)
        setLoading(false)
        setData(initialState)
      });
  };
  return (
    <>
      <div className="auth-wrapper">
        <div className="auth-content-new">
          <div class="auth-bg">
            <span className="r"></span>
            <span className="r s"></span>
            <span className="r s"></span>
            <span className="r"></span>
          </div>
          <div className="card">
            <div className="card-body text-center">
                <div class="mb-4">
                    <i class="feather icon-unlock auth-icon"></i>
                </div> 
              <h3 class="mb-4">Register</h3>
              <div className="row">
                <div className="col md-6">                                  
                    <div class="input-group mb-3">
                        First Name
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="first_name"
                        value={data.first_name}
                        onChange={handleInputChange}
                        />
                    </div> 
                    <div class="input-group mb-3">
                        Last Name
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="last_name"
                        value={data.last_name}
                        onChange={handleInputChange}
                        />
                    </div>
                    <div class="input-group mb-3">
                        Date of Birth
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="dob"
                        value={data.dob}
                        onChange={handleInputChange}
                        />
                    </div>  
                    <div class="input-group mb-3">
                        Gender
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="gender"
                        onChange={handleInputChange}
                        value={data.gender}
                        />
                    </div>         
                    <div class="input-group mb-3">
                        Address
                    </div>
                    <div class="input-group mb-4">
                        <textarea
                        class="form-control"
                        name="address"
                        onChange={handleInputChange}
                        value={data.address}
                        />
                    </div>                            
                    <div class="input-group mb-3">
                        E-mail
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="email"
                        class="form-control"
                        name="email"
                        onChange={handleInputChange}
                        value={data.email}
                        />
                    </div>
                    <div class="input-group mb-3">
                        Phone Number
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="phone_number"
                        onChange={handleInputChange}
                        value={data.phone_number}
                        />
                    </div>  
                    <div class="input-group mb-3">
                        NIK/Passport Number
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        name="nik_passport"
                        onChange={handleInputChange}
                        value={data.nik_passport}
                        />
                    </div>    
                </div>
                <div className="col md-6">
                    <div class="input-group mb-3">
                        Username
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="text"
                        class="form-control"
                        //   placeholder="username"
                        name="name"
                        value={data.name}
                        onChange={handleInputChange}
                        //   autoComplete="new-password"
                        />
                    </div>
                    <div class="input-group mb-3">
                        Password
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="password"
                        class="form-control"
                        onChange={handleInputChange}
                        value={data.password}
                        name="password"
                        />                        
                    </div>
                    <div class="input-group mb-1">
                        {errorMessage === '' ? null :
                        <span style={{
                        fontWeight: 'bold',
                        color: 'red',
                        }}>{errorMessage}</span>}
                    </div>
                    <div class="input-group mb-3">
                        Re-enter Password
                    </div>
                    <div class="input-group mb-4">
                        <input
                        type="password"
                        class="form-control"
                        name="confPassword"
                        onChange={handleInputChange}
                        value={data.confPassword}
                        />
                    </div>
                </div>
              </div>                
              {error && (
                <>
                  <small style={{ color: "red" }}>{error}</small>
                  <br />
                </>
              )}
              <br />
              {/* <button class="btn btn-primary shadow-2 mb-4">Login</button> */}
              <input
                type="button"
                className="btn btn-primary shadow-2 mb-4"
                value="Register"
                onClick={handleLogin}
                disabled={loading}
              />
              <br />
              {/* <p class="mb-2 text-muted">
                Forgot password? <a href="auth-reset-password.html">Reset</a>
              </p>
              <p class="mb-0 text-muted">
                Don’t have an account? <a href="auth-signup.html">Signup</a>
              </p> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

// const useFormInput = (initialValue) => {
//   const [value, setValue] = useState(initialValue);

//   const handleChange = (e) => {
//     setValue(e.target.value);
//   };
//   return {
//     value,
//     onChange: handleChange,
//   };
// };

export default Register;
