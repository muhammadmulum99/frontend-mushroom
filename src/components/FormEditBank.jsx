import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormEditBank = () => {
  const initialState = {
    uuid_master_bank: '',
    nama_master_bank: '',
    status_master_bank: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/bank/${id}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setData(decryptedData);
        }
        else{
          setData(initialState)
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [id]);

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/bank/${id}`, data);
      navigate("/bank");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Bank</h1>
      <h2 className="subtitle">Update Bank</h2>
      <div className="card is-shadowless">
        <div className="card-content">
        <div className="content">
            <form onSubmit={updateUser}>
              <p className="has-text-centered">{msg}</p>              
              <div className="field">
                <label className="label">Bank Name</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_master_bank"
                    value={data.nama_master_bank}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Status Bank</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="status_master_bank"
                    value={data.status_master_bank}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEditBank;
