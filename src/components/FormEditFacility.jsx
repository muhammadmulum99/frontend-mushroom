import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormEditFacility = () => {
  const initialState = {
    uuid_master_fasilitas: '',
    nama_master_fasilitas: '',
    icon_master_fasilitas: '',
    jenis_master_fasilitas: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/fasilitas/${id}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setData(decryptedData)
        }
        else{
          setData(initialState)
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [id]);

  const updateUser = async (e) => {
    e.preventDefault();
    try {
      await axios.patch(`http://localhost:5000/fasilitas/${id}`, data);
      navigate("/facility");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };
  return (
    <div>
      <h1 className="title">Facility</h1>
      <h2 className="subtitle">Update Facility</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={updateUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">ID Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="uuid_master_fasilitas"
                    value={data.uuid_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Nama Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_master_fasilitas"
                    value={data.nama_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Icon of Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="icon_master_fasilitas"
                    value={data.icon_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>
              <div className="field">
                <label className="label">Type of Master Facility</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="jenis_master_fasilitas"
                    value={data.jenis_master_fasilitas}
                    onChange={handleInputChange}
                    placeholder="Name"
                  />
                </div>
              </div>         

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Update
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEditFacility;
