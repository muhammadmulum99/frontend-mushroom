import React, { useEffect, useState, useRef } from "react";
import { Container, Row, Col, Form, Button, Card } from "react-bootstrap";
import "./contentpayment.css";
import { BsStar, BsDoorOpen } from "react-icons/bs";
import { MdOutlineDateRange } from "react-icons/md";
import { BiBed } from "react-icons/bi";
import logo from "./benderaindo.jpg";
import axios from "axios";
import { useNavigate,useParams } from "react-router-dom";
import { getMe } from "../../features/authSlice";
import { useDispatch, useSelector } from "react-redux";
import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";
import emailjs from "@emailjs/browser";
import cryptoJs from "crypto-js";

const Contentpayment = () => {
  // const [modal, setModal] = useState(false);
  // const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  
  const { data } = useParams()
  
  let detail = JSON.parse(data)
  
  let today = new Date();
  const initialState = {
    booking_id: "",
    jenis_kelamin: detail.jenis_kelamin,
    tgl_transaksi: today.getFullYear()+'-'+
    (today.getMonth() > 8 ? today.getMonth() + 1 : '' + today.getMonth() + 1)+'-'+
    (today.getDate() > 8 ? today.getDate() : ''+today.getDate()),
    name: detail.name,
    dob: detail.dob,
    email: detail.email,
    phone_number: detail.phone_number,
    nik_passport: detail.nik_passport,
    address: "",
    special_need: detail.special_need,
    room_name: "",
    id_properti_hotel: detail.id_properti_hotel,
    id_properti_kamar: detail.id_properti_kamar,
    room_type: detail.room_type,
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya: detail.total_biaya,
    room_price: detail.room_price,
    jumlah_kamar: detail.jumlah_kamar,
    jenis_hotel: detail.jenis_hotel,
    alamat_hotel: detail.alamat_hotel,
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: detail.tgl_checkin,
    tgl_checkout: detail.tgl_checkout,
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user,
    username: detail.username,
    bukti_pembayaran: '',
    email_property: ''
  }
  const navigate = useNavigate();
  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
  }

  useEffect(() => {
    const getUserById = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/propertihotel/${input.id_properti_hotel}`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setInput({
            ...input,
            email_property: decryptedData.email_hotel
          });
        }
      } catch (error) {
        if (error.response) {
          alert(error.response.data.msg);
        }
      }
    };
    getUserById();
  }, [input.id_properti_hotel]);

  const saveUser = async (e) => {
    e.preventDefault();    
    try {
      const response = await axios.post("http://localhost:5000/transaksi", input);
      emailjs.send("service_92jz21f","template_lkrz283",{
        email: input.email,
        name: input.name,
        jenis_hotel: input.jenis_hotel,
        id_transaksi: response.data.msg,
        tgl_transaksi: input.tgl_transaksi,
        room_type: input.room_type,
        room_price: input.room_price,
        jumlah_kamar: input.jumlah_kamar,
        total_biaya: input.total_biaya, 
        email_property: input.email_property,       
        status: input.jenis_pembayaran === 'COD' ? 'MENUNGGU PEMBAYARAN' : 'LUNAS'
      },"F0FZPc_8tzDxXO61Q")
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
      downloadPdfDocument()
      navigate("/");
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // downloadPdfDocument()
  };

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      setInput({...input, id_user: 'MUSHROOMGUEST', username: 'MUSHROOMGUEST'})
      // setShow(true)
    }
    if(user){
      setInput({...input, id_user: user.uuid, username: user.user_name})
      // setShow(false)
    }
  }, [isError, user, navigate]);

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setInput({
          ...input,
          bukti_pembayaran: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  const downloadPdfDocument = () => {
    const input = document.getElementById("testId");
    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF();
            pdf.addImage(imgData, 'JPEG', 0, 0);
            pdf.save("struktest.pdf");
        })
  }

  return (
    <>
      <div className="main-content-payment">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={12} md={8} style={{}}>
              <Card
                style={{
                  width: "600px",
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                }}
              >
                <div className="main-contentpayment">
                  <p>Payment</p>
                </div>
                <Card.Body >
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Name: {input.name}</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Date of Birth: {input.dob}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Gender: {input.jenis_kelamin}</Form.Label>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Email: {input.email}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Phone Number: {input.phone_number}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>NIK/Passport Number: {input.nik_passport}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Special Needs: {input.special_need}</Form.Label>                      
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Promo</Form.Label>
                      <Form.Control type="text" placeholder="Promo Code" name="kode_promo" onChange={handleInputChange} />
                    </Form.Group>
                    {/* <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Payment Method</Form.Label>
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                      <Form.Check type="checkbox" label="Mandiri" />
                    </Form.Group> */}                    
                    <Form.Group className="mb-3">
                      <Form.Label>Payment Method</Form.Label>
                      <Form.Select name="jenis_pembayaran" onChange={handleInputChange}>
                        <option value="">Pilih Pembayaran</option>
                        <option>COD</option>
                        <option>Bank BRI</option>
                        <option>Bank Mandiri</option>
                        <option>Bank BCA</option>
                        <option>Bank BNI</option>
                        <option>Bank Permata</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Proof of Payment</Form.Label>
                      <Form.Control type="file" placeholder="Promo Code" name="bukti_pembayaran" onChange={uploadFiles} />
                    </Form.Group>
                    <div
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Button variant="outline-warning" onClick={saveUser}>Pay Now</Button>{" "}
                    </div>
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={6} md={4} style={{ position: "sticky" }}>
              <Card
                id="testId"
                style={{
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                  width: "450px",
                }}
              >
                <Card.Body>
                  <div className="main-contentkiriform">
                    <div className="contentkiri-fotonama">
                      <img src={logo} alt="" />
                      <div className="hotelnama">
                        <h3>{input.jenis_hotel}</h3>
                        <p>{input.alamat_hotel}</p>
                      </div>
                    </div>
                    <div className="contentkiri-border">
                      <div className="contentkiri-checkin">
                        <div className="checkin">
                          <h3>Check In</h3>
                          <p>{input.tgl_checkin}</p>
                        </div>
                        <div className="checkout">
                          <h3>Check Out</h3>
                          <p>{input.tgl_checkout}</p>
                        </div>
                      </div>
                      <hr align="left" width="394" />
                      <div className="contentkiri-room">
                        <div className="contentkiri-roomtype">
                          <h3>Room type</h3>
                          <p>{input.room_type}</p>
                        </div>
                        <div className="contentkiri-roomroom">
                          <h3>Room</h3>
                          <p>x {input.jumlah_kamar}</p>
                        </div>
                      </div>
                    </div>
                    <div className="contentkiri-roomprice">
                      <h3>Room price</h3>
                      <p>Rp. {input.room_price}</p>
                    </div>
                    <div className="contentkiri-ppn">
                      <h3>PPN</h3>
                      <p>{input.ppn}</p>
                    </div>
                    <div className="contentkiri-fee">
                      <h3>Platform fee</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-promo">
                      <h3>Promo</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-refund">
                      <h3>Refund</h3>
                      <p>Not Availble</p>
                    </div>
                    <hr align="left" width="394" />
                    <div className="contentkiri-price">
                      <h3>Total Price</h3>
                      <p>Rp. {input.total_biaya}</p>
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
    </>
  );
};

export default Contentpayment;
