// import React from "react";
import React, { useState, useEffect } from "react";
import "./contentdetailhotel.css";
import "./hoteldetail.css";
import { Container, Row, Col, Card, Form, Modal, Table } from "react-bootstrap";
// import Carousel from "react-multi-carousel";
// import WithStyles from "react-multi-carousel";
// import "react-multi-carousel/lib/styles.css";
import logo from "./benderaindo.jpg";
import { BsStar, BsDoorOpen } from "react-icons/bs";
import { MdOutlineDateRange } from "react-icons/md";
import { BiBed } from "react-icons/bi";
import { Hotel } from "./benderaindo.jpg";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import cryptoJs from "crypto-js";
import { useDispatch, useSelector } from "react-redux";
import { getMe, LoginUser } from "../../features/authSlice";
import swal from "sweetalert";


const Contentdetailhotel = () => {
  const [modal, setModal] = useState(false);
  // const [show, setShow] = useState(false);
  let detail = localStorage.getItem('detail') ? JSON.parse(localStorage.getItem('detail')) :
  {
    id_properti_hotel: '', 
    id_properti_kamar: '',
    tgl_checkin: '',
    tgl_checkout: '',
    total_biaya: '',
    jumlah_kamar: '',
    room_type: '',
    id_user: "",
    username: "",
  }

  const { id, checkin, checkout } = useParams()

  const [foto, setFoto] = useState([]);
  const [kamar, setKamar] = useState([]);
  const [comms, setComms] = useState([]);

  const initialState = {
    id_properti_hotel: id, 
    id_properti_kamar: '',
    jenis_hotel: '',
    alamat_hotel: '',
    tgl_checkin: checkin,
    tgl_checkout: checkout,
    total_biaya: '',
    jumlah_kamar: '',
    room_type: '',
    id_user: '',
    email_hotel: '',
    username: '',
    harga_kamar: '',
    ulasan: '',
    title_ulasan: '',
    rating: '',
    sisa_kamar: '0'
  }

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
  }

  const navigasi = useNavigate();
  const handlebook = () => {
    // localStorage.setItem('transaksi', JSON.stringify(input))
    navigasi("/transaction/"+JSON.stringify(input));
  };

  useEffect(() => {
    if(input.id_properti_hotel === '') return;
    getHotel();
    getKamar();
    getComms();
    getFoto();
  }, [input.id_properti_hotel]);

  useEffect(() => {
    if(input.id_properti_kamar === '') return;
    setInput({
      ...input,
      room_type: kamar.length > 0 ? kamar.filter((fil) => fil.uuid_properti_kamar === input.id_properti_kamar && fil.id_properti_hotel === input.id_properti_hotel)[0].type_kamar : ''
    })
  }, [input.id_properti_kamar]);

  const handleHarga = () => {
    setInput({
      ...input,
      total_biaya: parseFloat(input.jumlah_kamar) * parseFloat(input.harga_kamar)
    })    
  }

  useEffect(() => {
    if(input.jumlah_kamar === '') return;
    handleHarga();
  }, [input.jumlah_kamar]);

  const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/propertihotel/${input.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    // setHotel(decryptedData);
    if(decryptedData){
      setInput({
        ...input,
        alamat_hotel: decryptedData.alamat_hotel,
        jenis_hotel: decryptedData.jenis_hotel,
      })
    }
  };

  const getFoto = async () => {
    const response = await axios.get(`http://localhost:5000/fotokamar/${input.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    // setHotel(decryptedData);
    if(decryptedData){
      setFoto(decryptedData)
    }
  };

  const getKamar = async () => {
    const response = await axios.get(`http://localhost:5000/getsisakamar/${input.id_properti_hotel}/${input.tgl_checkin}/${input.tgl_checkout}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    if(decryptedData){
      setKamar(decryptedData);
    }
  };

  const getComms = async () => {
    const response = await axios.get(`http://localhost:5000/ratinguser/${input.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    // console.log(decryptedData)
    if(decryptedData){
      setComms(decryptedData);
    }
  };

  const handleComment = async () => {
    try {
      await axios.post("http://localhost:5000/ratinguser", input);
      getComms();
    } catch (error) {
      if (error.response) {
        // alert(error.response.data.msg);
        // navigasi("/login");
        setModal(true)
      }
    }
  }

  const dispatch = useDispatch();
  const { user, isError, isSuccess, isLoading, message } = useSelector(
    (state) => state.auth
  );

  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (user || isSuccess) {
      setInput({...input, id_user: user.uuid, username: user.user_name})
      setModal(false)
    }
  }, [isError, user, navigate]);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ email, password }));
  };

  const [sisa, setSisa] = useState([])

  useEffect(() => {
    if(input.id_properti_kamar === '') return;
    let date = new Date(input.tgl_checkin)
    setSisa([])
    setInput({
      ...input,
      harga_kamar: date.getDay() > 4 ? kamar.filter((fil) => fil.uuid_properti_kamar === input.id_properti_kamar && fil.id_properti_hotel === input.id_properti_hotel)[0].harga_weekend : kamar.filter((fil) => fil.uuid_properti_kamar === input.id_properti_kamar && fil.id_properti_hotel === input.id_properti_hotel)[0].harga_weekday,
      sisa_kamar: kamar.filter((item) => item.uuid_properti_kamar === input.id_properti_kamar)[0].get_sisa_kamar,
      room_type: kamar.filter((item) => item.uuid_properti_kamar === input.id_properti_kamar)[0].type_kamar
    })
  },[input.id_properti_kamar])

  useEffect(() => {
    let temp = []
    for(let i = 0; i < parseInt(input.sisa_kamar); i++){
      temp.push({
        id: (i+1).toString()
      })
    }
    setSisa(temp)
  },[input.sisa_kamar])
 
  const handleWishlist = async () => {
    try {
      const response = await axios.post("http://localhost:5000/keranjang", {
        id_user: input.id_user,
        id_properti_hotel: input.id_properti_hotel,
        id_properti_kamar: input.id_properti_kamar,
        tgl_checkin: input.tgl_checkin,
        tgl_checkout: input.tgl_checkout,
        total_biaya: input.total_biaya,
        jumlah_kamar: input.jumlah_kamar,
        room_type: input.room_type,
      });
      alert(response.data.msg)
      // navigate("/wishlist");
    } catch (error) {
      if(input.id_user === ''){
        setModal(true)
      }
      else if(input.id_properti_kamar === ''){
        alert('Please pick a room...')
      }
      else if(input.jumlah_kamar === ''){
        alert('Please pick how many rooms you would like to stay...')
      }
      // else{
      //   // alert(error.response.data.msg);
      //   setModal(true)
      // }
    }
  }

  return (
    <>
      <div className="main-content-detailhotels">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={12} md={8} style={{}}>
              {/* <div className="coba">
                <Row>
                  <Col style={{}}>
                    <Card.Img variant="top" src={logo} />
                  </Col>
                </Row>
                <Row>
                  <Col style={{}}>
                    <Card.Img
                      variant="top"
                      src={logo}
                      style={{ marginTop: "15px", marginLeft: "8px" }}
                    />
                  </Col>
                  <Col style={{}}>
                    <Card.Img
                      variant="top"
                      src={logo}
                      style={{ marginTop: "15px", marginRight: "8px" }}
                    />
                  </Col>
                </Row>
              </div> */}

              {/* bawah galeri */}
              <div className="contentkiri-name">
                <h3>{input.jenis_hotel}</h3>
                <div className="coba">
                  <span>
                    <BsStar /> 3<p></p>
                  </span>
                </div>
              </div>

              <div className="contentkiri-lokasi">
                <h3>Location</h3>
                <p>{input.alamat_hotel}</p>
              </div>

              <div className="contentkiri-fasilitas">
                <h3>Facility</h3>
              </div>
              <div class="container carousel-inner no-padding">
                <div class="carousel-item active">
                  {foto.map((item,key) => {
                    return(
                      item.foto_fasilitas_kamar === 'xxx'|| null ? null :
                      <div class="col-xs-3 col-sm-3 col-md-3">
                        <img src={"data:image/jpeg;base64," + item.foto_fasilitas_kamar} />
                      </div>
                    )
                  })}
                </div>                  
              </div>

              {/* <div className="contentkiri-fasilitasicon">
                
              </div> */}

              {/* <div className="contentkiri-rooms">
                <h3>Rooms</h3>
              </div> */}

              <div style={{ justifyContent: "center" }}>
                {/* <!-- Indicators --> */}
                {/* <ul class="carousel-indicators">
                  <li data-target="#demo" data-slide-to="0" class="active"></li>
                  <li data-target="#demo" data-slide-to="1"></li>
                  <li data-target="#demo" data-slide-to="2"></li>
                </ul> */}

                {/* <!-- The slideshow --> */}
                <Table striped hover size="md">
                    <thead>
                      <tr>
                        <th>Room type</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>View</th>                         
                      </tr>
                    </thead>
                    <tbody>                 
                        {kamar.map((item,key) => {
                          return(
                            <tr>
                              <td>{item.type_kamar}</td>
                              <td>{new Date().getDay() > 4 ? item.harga_weekend : item.harga_weekday}</td>  
                              <td>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Veniam consequatur reiciendis totam cum eos amet est sequi
                                earum ex. Impedit odio saepe reprehenderit doloribus. Ullam
                                necessitatibus perferendis sit optio et.
                              </td>
                              <td className="col-xs-3 col-sm-3">
                                <img src={item.foto_kamar ? "data:image/jpeg;base64," + item.foto_kamar: "https://cf.bstatic.com/xdata/images/hotel/square600/261707778.webp?k=fa6b6128468ec15e81f7d076b6f2473fa3a80c255582f155cae35f9edbffdd78&o=&s=1"} />
                              </td>
                              
                            </tr>                     
                          )
                        })}
                    </tbody>
                  </Table> 

                {/* <!-- Left and right controls --> */}
                {/* <a class="carousel-control-prev" href="#demo" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
                </a> */}
              </div>

              <div className="contentkiri-desc">
                <h3>Hotel Description</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Veniam consequatur reiciendis totam cum eos amet est sequi
                  earum ex. Impedit odio saepe reprehenderit doloribus. Ullam
                  necessitatibus perferendis sit optio et.
                </p>
              </div>

              <div className="contentkiri-policy">
                <h3>Hotel Policy</h3>
                <span> Lorem, ipsum dolor sit amet consectetur</span>
                <br />
                <span>Lorem, ipsum dolor sit amet consectetur</span>
                <br />
                <span>Lorem, ipsum dolor sit amet consectetur</span>
              </div>

              <div className="contentkiri-riview">
                <h3>Customer Reviews</h3>
                {/* <div className="coba-riview"> */}
                <span>(123)</span>
                {/* </div> */}                
              </div>
              
              <Row>
                <div className="siDesc">
                  <input name="title_ulasan" type="text" onChange={handleInputChange}/>
                  <input name="ulasan" type="textarea" onChange={handleInputChange}/>
                  <input name="rating" type="text" onChange={handleInputChange}/>
                  <select name="id_properti_kamar" onChange={handleInputChange}>
                    <option value="">Pilih Kamar</option>
                    {kamar.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_properti_kamar}>{item.type_kamar}</option>
                      )
                    })}
                  </select>
                  <button className="siCheckButton" onClick={handleComment}>
                    Add Comment
                  </button>
                </div>
              </Row>

              <div> 
                <Table responsive>
                  <tbody>                 
                    <th>
                      {comms.map((item,key) => {
                        return(
                          <td>
                            <Card>
                              <Card.Body>
                                <Card.Title>{item.title_ulasan}</Card.Title>
                                <Card.Text>
                                  {item.ulasan}
                                </Card.Text>
                              </Card.Body>
                            </Card>
                          </td>
                        )
                      })}
                    </th>
                  </tbody>
                </Table>                               
              </div>

              <div>
                <h3>Nearbi Hotel</h3>
              </div>

              <div style={{ justifyContent: "center" }}>
                <div class="container carousel-inner no-padding">
                  <div class="carousel-item active">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                      <img src={logo} />
                    </div>
                  </div>
                </div>

              
              </div>

              <div>
                {/* <Carousel
                  additionalTransfrom={0}
                  arrows
                  autoPlaySpeed={3000}
                  centerMode
                  className=""
                  containerClass="container"
                  dotListClass=""
                  draggable
                  focusOnSelect={false}
                  infinite
                  itemClass=""
                  keyBoardControl
                  minimumTouchDrag={80}
                  pauseOnHover
                  renderArrowsWhenDisabled={false}
                  renderButtonGroupOutside={false}
                  renderDotsOutside={false}
                  responsive={{
                    desktop: {
                      breakpoint: {
                        max: 3000,
                        min: 1024,
                      },
                      items: 3,
                      partialVisibilityGutter: 40,
                    },
                    mobile: {
                      breakpoint: {
                        max: 464,
                        min: 0,
                      },
                      items: 1,
                      partialVisibilityGutter: 30,
                    },
                    tablet: {
                      breakpoint: {
                        max: 1024,
                        min: 464,
                      },
                      items: 2,
                      partialVisibilityGutter: 30,
                    },
                  }}
                  rewind={false}
                  rewindWithAnimation={false}
                  rtl={false}
                  shouldResetAutoplay
                  showDots={false}
                  sliderClass=""
                  slidesToSlide={1}
                  swipeable
                >
                  <WithStyles
                    description="Fixing CSS load order/style.chunk.css incorrect in Nextjs"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="Appending currency sign to a purchase form in your e-commerce site using plain JavaScript."
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1549396535-c11d5c55b9df?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 2"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="Appending currency sign to a purchase form in your e-commerce site using plain JavaScript."
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 2"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 1"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550223640-23097fc71cb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 1"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="Fixing CSS load order/style.chunk.css incorrect in Nextjs"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 2"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="Appending currency sign to a purchase form in your e-commerce site using plain JavaScript."
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="Appending currency sign to a purchase form in your e-commerce site using plain JavaScript."
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                  <WithStyles
                    description="React Carousel with Server Side Rendering Support – Part 2"
                    headline="w3js.com - web front-end studio"
                    image="https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
                  />
                </Carousel> */}

                {/* <Carousel
                  ssr
                  partialVisbile
                  itemClass="image-item"
                  responsive={responsive}
                >
                  {images.slice(0, 5).map((image) => {
                    return (
                      <Image
                        draggable={false}
                        style={{ width: "100%", height: "100%" }}
                        src={image}
                      />
                    );
                  })}
                </Carousel> */}

                {/* <Container>
                  <Carousel variant="dark">
                    <Carousel.Item>
                      <img
                        className="d-block w-100"
                        src="holder.js/800x400?text=First slide&bg=f5f5f5"
                        alt="First slide"
                      />
                      <Carousel.Caption>
                        <h5>First slide label</h5>
                        <p>
                          Nulla vitae elit libero, a pharetra augue mollis
                          interdum.
                        </p>
                      </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                      <img
                        className="d-block w-100"
                        src="holder.js/800x400?text=Second slide&bg=eee"
                        alt="Second slide"
                      />
                      <Carousel.Caption>
                        <h5>Second slide label</h5>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit.
                        </p>
                      </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                      <img
                        className="d-block w-100"
                        src="holder.js/800x400?text=Third slide&bg=e5e5e5"
                        alt="Third slide"
                      />
                      <Carousel.Caption>
                        <h5>Third slide label</h5>
                        <p>
                          Praesent commodo cursus magna, vel scelerisque nisl
                          consectetur.
                        </p>
                      </Carousel.Caption>
                    </Carousel.Item>
                  </Carousel>
                </Container> */}
              </div>
            </Col>

            <Col xs={6} md={4} style={{ position: "sticky" }}>
              <Card
                style={{
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                }}
              >
                <Card.Body>
                  <div className="formbookharga-main">
                    <div className="fromharga-harga">
                      <h3>
                        <i>Order Summary</i>
                      </h3>
                    </div>

                    <div className="formharga-rating">
                      <p>
                        <span>
                          <BsStar />
                        </span>
                        <b>3/5</b>
                      </p>
                      <p>(123)</p>
                    </div>
                  </div>

                  {/* <div className="formbook-form">
                    <div className="formbook-checkin">
                      <h3>Check in date</h3>
                      <input type='date' name='tgl_checkin' onChange={handleInputChange} />
                    </div>
                  </div>
                  <div className="formbook-form">
                    <div className="formbook-checkin">
                      <h3>Check out date</h3>
                      <input type='date' name='tgl_checkout' onChange={handleInputChange} />
                    </div>
                  </div> */}
                  <div className="formbook-form">
                    <div className="formbook-checkin">
                      <h3>Room type</h3>
                      <select name="id_properti_kamar" onChange={handleInputChange} value={input.id_properti_kamar}>
                        <option value="">Pilih Kamar</option>
                        {kamar.map((item,key) => {
                          return(
                            <option key={key} value={item.uuid_properti_kamar}>{item.type_kamar}</option>
                          )
                        })}
                      </select>
                      {/* <i>
                        <BiBed />
                      </i> */}
                    </div>
                  </div>
                  <div className="formbook-form">
                    <div className="formbook-checkin">
                      <h3>Number of room</h3>
                      <select name="jumlah_kamar" onChange={handleInputChange}>
                            <option value="">Choose how many rooms</option>
                        {sisa.map((item,key) => {
                          return(
                            <option key={key} value={item.id}>{item.id} - Rp.{parseInt(item.id)*parseInt(input.harga_kamar)}</option>
                          )
                        })}
                      </select>
                      {/* <i>
                        <BsDoorOpen />
                      </i> */}
                    </div>
                  </div>
                  <hr align="left" width="390" />

                  <div className="formbook-totalprice">
                    <h3>Total Price :</h3>
                    <h3>Rp. {input.total_biaya ? input.total_biaya : '0'}</h3>
                  </div>
                  <div className="formbook-button">
                    <button onClick={handlebook}>Book now</button>
                  </div>
                  <div className="formbook-button">
                    <button onClick={handleWishlist}>Add to wishlist</button>
                  </div>
                  {/* <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Check in Date</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Check Out Date</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Room Type</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Number of Rooms</Form.Label>
                      <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Label style={{ marginBottom: "20px" }}>
                      Order Detail :{" "}
                    </Form.Label>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Hotel Name : </Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        hidden
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Stay Duration : </Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        hidden
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Room Type :</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        hidden
                      />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Number of rooms :</Form.Label>
                      <Form.Control
                        type="email"
                        placeholder="Enter email"
                        hidden
                      />
                    </Form.Group>
                  </Form>
                  <Button variant="warning" type="submit" size="lg">
                    Submit
                  </Button> */}

                  {/* <div
                    className="BookingPanel_hoteldisRight_2RqlxxJzZr-26SMThuRm8h"
                    id="HotelInfoRight"
                    style={{
                      position: "sticky",
                      top: "8vw",
                      marginTop: "33px",
                    }}
                  >
                    <div className="BookingPanel_hotelBookingDis_2mjzPESfYV5YeRL93OcLdR">
                      <div className="BookingPanel_hotelPriceDiscount_2vqHTR0vkREfNlXabA1Dvo">
                        <p></p>
                        <h3>
                          <i>Rp</i>
                          <i>220.000</i>
                        </h3>
                      </div>
                      <div className="BookingPanel_hotelPriceStar_2mxu2rMPVlvTO_YONBJi1N">
                        <p>
                          <span>
                            <img
                              fetchpriority="auto"
                              src="https://massets.reddoorz.com/images/listing/bigstar.png"
                              width="auto"
                              height="auto"
                              title=""
                              loading="auto"
                            />
                          </span>
                          <b>3.9</b>
                        </p>
                        <p className="BookingPanel_countValue_3gKYSyFeR-o5EPSw1rn5Cm">
                          (307)
                        </p>
                      </div>
                    </div>
                    {/* batas */}
                  {/* <div className="BookingPanel_checkValue_2gd9UFI0uVCeTa5vjdYcPR"> */}
                  {/* <div className="BookingPanel_checkTime_28BtPjC21uE8LXnReGt0Ef">
                    <div className="BookingPanel_checkIn_b0eTz1YxlugwElrmfrMH-">
                      <h3>
                        <span data-message-id="check_in">Check In</span>
                      </h3>
                      <h4>
                        <span data-message-id="check_in_day">Thu, 20 Oct</span>
                      </h4>
                    </div>
                    <div>
                      <h3>
                        <span data-message-id="check_out">Check Out</span>
                      </h3>
                      <h4>
                        <span data-message-id="check_out_day">Fri, 21 Oct</span>
                      </h4>
                    </div>
                  </div> */}
                  {/* <div className="BookingPanel_roomCount_3g0-U0g0qhvqnSsv652FxC">
                    <h3>
                      <span data-message-id="room_head">Room(s)</span>
                    </h3>
                    <h4>
                      <span data-message-id="room_count">1</span>
                    </h4>
                  </div> */}
                  {/* </div> */}
                  {/* batass */}
                  {/* <div className="BookingPanel_checkValue_2gd9UFI0uVCeTa5vjdYcPR BookingPanel_checkValueOverride_3cVc7zvQoJfa8PMWCVlxCG">
                      <div className="BookingPanel_checkTimeIn_2mLZKKiwxuOPDA38JVf3Om">
                        <div className="BookingPanel_checkIn_b0eTz1YxlugwElrmfrMH-">
                          <h3>
                            <span data-message-id="room_type">Room Type</span>
                          </h3>
                          <h4>Sale Room</h4>
                        </div>
                      </div>
                    </div> */}
                  {/* batas */}
                  {/* <div className="BookingPanel_totalSaving_30iAh7bMSLEYoTR1i04I-t">
                      <div className="BookingPanel_checkIn_b0eTz1YxlugwElrmfrMH-">
                        <h3>
                          <span data-message-id="total_price">Total Price</span>
                        </h3>
                      </div>
                      <div>
                        <h2>
                          <i>Rp</i> <i>220.000</i>
                        </h2>
                      </div>
                    </div> */}
                  {/* batas */}
                  {/* <button
                      className="BookingPanel_bookNowBtn_3rvmCTVXRi3z9xJNRXrBD-"
                      style={{ width: "100%" }}
                    >
                      <span>BOOK NOW</span>
                    </button> */}
                  {/* </div>
                   */}
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
      </div>
      <Modal show={modal} onHide={() => setModal(false)}>
        <Modal.Body>
          <div class="card">
            <div class="card-body text-center">
              <div class="mb-4">
                <i class="feather icon-unlock auth-icon"></i>
              </div>
              <h3 class="mb-4">Login</h3>
              <div class="input-group mb-3">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Email or username"
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              <div class="input-group mb-4">
                <input
                  type="password"
                  class="form-control"
                  placeholder="password"
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              {isError && <p className="has-text-centered" style={{ color: 'red' }}>{message}</p>}
              <div class="form-group text-left">
                <div class="checkbox checkbox-fill d-inline">
                  <input
                    type="checkbox"
                    name="checkbox-fill-1"
                    id="checkbox-fill-a1"
                    checked=""
                  />
                  <label for="checkbox-fill-a1" class="cr">
                    {" "}
                    Save Details
                  </label>
                </div>
              </div>
              {/* {error && (
                <>
                  <small style={{ color: "red" }}>{error}</small>
                  <br />
                </>
              )} */}
              <br />
              {/* <button class="btn btn-primary shadow-2 mb-4">Login</button> */}
              <input
                type="button"
                class="btn btn-primary shadow-2 mb-4"
                disabled={isLoading ? true : false}
                value={isLoading ? "Loading..." : "Login"}
                onClick={Auth}
              />
              <br />
              <p class="mb-2 text-muted">
                Forgot password? <a href="auth-reset-password.html">Reset</a>
              </p>
              <p class="mb-0 text-muted">
                Don’t have an account? <a href="/register">Signup</a>
              </p>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Contentdetailhotel;
