import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Card, Modal } from "react-bootstrap";
import "./contenttransaksi.css";
import { BsStar, BsDoorOpen } from "react-icons/bs";
import { MdOutlineDateRange } from "react-icons/md";
import { BiBed } from "react-icons/bi";
import logo from "./benderaindo.jpg";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getMe, LoginUser } from "../../features/authSlice";

const Contenttransaksi = () => {
  const [modal, setModal] = useState(false);
  const [show, setShow] = useState(false);
   
  const navigasi = useNavigate();

  const { data } = useParams()
  let detail = JSON.parse(data)

  const initialState = {
    booking_id: "",
    name: "",
    dob: "",
    email: "",
    phone_number: "",
    nik_passport: "",
    address: "",
    special_need: "",
    room_name: "",
    jenis_kelamin: '',
    id_properti_hotel: detail.id_properti_hotel,
    id_properti_kamar: detail.id_properti_kamar,
    room_type: detail.room_type,
    room_number: "",
    start_date: "",
    end_date: "",
    total_biaya:  (parseInt(detail.harga_kamar) * parseInt(detail.jumlah_kamar) * ((new Date(detail.tgl_checkout) - new Date(detail.tgl_checkin))/((24*60*60*1000)))).toString(),
    jumlah_kamar: detail.jumlah_kamar,
    room_price: detail.harga_kamar,
    jenis_hotel: detail.jenis_hotel,
    alamat_hotel: detail.alamat_hotel,
    additions: "",
    platfrom_fee: "",
    ppn: "",
    tgl_checkin: detail.tgl_checkin,
    tgl_checkout: detail.tgl_checkout,
    no_ref: "",
    sumber: "",
    kode_promo: "",
    jenis_pembayaran: "",
    id_user: detail.id_user,
    username: detail.username
  }  

  const [input, setInput] = useState(initialState);

  const handleInputChange = (event) => {
    setInput({
      ...input,
      [event.target.name]: event.target.value,
    });
  }
  const handletransaksi = () => {
    // localStorage.setItem('payment', JSON.stringify(input))
    navigasi("/payment/"+JSON.stringify(input));
  };

  const dispatch = useDispatch();
  const { user, isError, isSuccess, isLoading, message } = useSelector(
    (state) => state.auth
  );
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      setInput({...input, id_user: 'MUSHROOMGUEST', username: 'MUSHROOMGUEST'})
      setShow(true)
    }
    if(user || isSuccess){
      setInput({...input, id_user: user.uuid, username: user.user_name})
      setShow(false)
      setModal(false)
    }
  }, [isError, user, navigate]);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const Auth = (e) => {
    e.preventDefault();
    dispatch(LoginUser({ email, password }));
  };

  return (
    <>
      <div className="main-content-transaksi">
        <Container>
          {/* Stack the columns on mobile by making one full-width and the other half-width */}
          <Row>
            <Col xs={12} md={8} style={{}}>
              <Card
                style={{
                  width: "600px",
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                }}
              >
                <div className="main-contenttransaksi">
                  <p>Transaction Details</p>
                </div>
                <Card.Body>
                  <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                      <Form.Label>Name</Form.Label>
                      <Form.Control type="text" name="name" onChange={handleInputChange} placeholder="Full Name" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Date of Birth</Form.Label>
                      <Form.Control type="date" name="dob" onChange={handleInputChange} placeholder="Date of Birth" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                      <Form.Label>Gender</Form.Label>
                      <Form.Select name="jenis_kelamin" onChange={handleInputChange}>
                        <option value="">Please choose</option>
                        <option>Male</option>
                        <option>Female</option>
                      </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Email</Form.Label>
                      <Form.Control type="email" name="email" onChange={handleInputChange} placeholder="Email" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Phone Number</Form.Label>
                      <Form.Control type="text" name="phone_number" onChange={handleInputChange} placeholder="Phone Number" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>NIK/Passport Number</Form.Label>
                      <Form.Control
                        type="text"
                        name="nik_passport" onChange={handleInputChange}
                        placeholder="NIK/Passport Number"
                      />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                      <Form.Label>Social Needs</Form.Label>
                      <Form.Control type="text" name="special_needs" onChange={handleInputChange} placeholder="Special Needs" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                      <Form.Check
                        type="checkbox"
                        label="I have read and accept the Terms and Conditions"
                      />
                    </Form.Group>
                    <Button variant="outline-warning" onClick={handletransaksi}>
                      Pay {show ? 'as Guest' : null}
                    </Button>{" "}
                    {show ? <Button variant="warning" onClick={() => setModal(true)}>Login</Button> : null}
                    {" "}
                  </Form>
                </Card.Body>
              </Card>
            </Col>

            <Col xs={6} md={4} style={{ position: "sticky" }}>
              <Card
                style={{
                  boxShadow: "0 10px 40px #0000004d",
                  borderRadius: "30px",
                  position: "sticky",
                  width: "450px",
                }}
              >
                <Card.Body>
                  <div className="main-contentkiriform">
                    <div className="contentkiri-fotonama">
                      <img src={logo} alt="" />
                      <div className="hotelnama">
                        <h3>{input.jenis_hotel}</h3>
                        <p>{input.alamat_hotel}</p>
                      </div>
                    </div>
                    <div className="contentkiri-border">
                      <div className="contentkiri-checkin">
                        <div className="checkin">
                          <h3>Check In</h3>
                          <p>{input.tgl_checkin}</p>
                        </div>
                        <div className="checkout">
                          <h3>Check Out</h3>
                          <p>{input.tgl_checkout}</p>
                        </div>
                      </div>
                      <hr align="left" width="394" />
                      <div className="contentkiri-room">
                        <div className="contentkiri-roomtype">
                          <h3>Room type</h3>
                          <p>{input.room_type}</p>
                        </div>
                        <div className="contentkiri-roomroom">
                          <h3>Room</h3>
                          <p>x {input.jumlah_kamar}</p>
                        </div>
                      </div>
                    </div>
                    <div className="contentkiri-roomprice">
                      <h3>Room price</h3>
                      <p>Rp. {input.room_price}</p>
                    </div>
                    <div className="contentkiri-ppn">
                      <h3>PPN</h3>
                      <p>{input.ppn}</p>
                    </div>
                    <div className="contentkiri-fee">
                      <h3>Platform fee</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-promo">
                      <h3>Promo</h3>
                      <p>{}</p>
                    </div>
                    <div className="contentkiri-refund">
                      <h3>Refund</h3>
                      <p>Not Availble</p>
                    </div>
                    <hr align="left" width="394" />
                    <div className="contentkiri-price">
                      <h3>Total Price</h3>
                      <p>Rp. {input.total_biaya}</p>
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </Col>
          </Row>

          {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}

          {/* Columns are always 50% wide, on mobile and desktop */}
        </Container>
        {/*  */}
      </div>
      <Modal show={modal} onHide={() => setModal(false)}>
        <Modal.Body>
          <div class="card">
            <div class="card-body text-center">
              <div class="mb-4">
                <i class="feather icon-unlock auth-icon"></i>
              </div>
              <h3 class="mb-4">Login</h3>
              <div class="input-group mb-3">
                <input
                  type="text"
                  class="form-control"
                  placeholder="Email or username"
                  onChange={(e) => setEmail(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              <div class="input-group mb-4">
                <input
                  type="password"
                  class="form-control"
                  placeholder="password"
                  onChange={(e) => setPassword(e.target.value)}
                  autoComplete="new-password"
                />
              </div>
              {isError && <p className="has-text-centered" style={{ color: 'red' }}>{message}</p>}
              <div class="form-group text-left">
                <div class="checkbox checkbox-fill d-inline">
                  <input
                    type="checkbox"
                    name="checkbox-fill-1"
                    id="checkbox-fill-a1"
                    checked=""
                  />
                  <label for="checkbox-fill-a1" class="cr">
                    {" "}
                    Save Details
                  </label>
                </div>
              </div>
              {/* {error && (
                <>
                  <small style={{ color: "red" }}>{error}</small>
                  <br />
                </>
              )} */}
              <br />
              {/* <button class="btn btn-primary shadow-2 mb-4">Login</button> */}
              <input
                type="button"
                class="btn btn-primary shadow-2 mb-4"
                disabled={isLoading ? true : false}
                value={isLoading ? "Loading..." : "Login"}
                onClick={Auth}
              />
              <br />
              <p class="mb-2 text-muted">
                Forgot password? <a href="auth-reset-password.html">Reset</a>
              </p>
              <p class="mb-0 text-muted">
                Don’t have an account? <a href="/register">Signup</a>
              </p>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Contenttransaksi;
