import React from "react";
import { Navbar, Container } from "react-bootstrap";
import { GrFacebookOption } from "react-icons/gr";
import { BsInstagram } from "react-icons/bs";
import { FaTiktok } from "react-icons/fa";
import { BsTwitter } from "react-icons/bs";
import { BsYoutube } from "react-icons/bs";
const Footeruser = () => {
  return (
    <>
      <footer 
      // style={{ marginTop: "430px" }}
      >
        <div className="footer">
          <Navbar bg="light" fixed="bottom">
            <Container>
              <div className="soscial-footer">
                <a
                  href=""
                  style={{
                    marginRight: "7px",
                    color: "black",
                    fontSize: "23px",
                  }}
                >
                  <GrFacebookOption />
                </a>
                <a
                  href=""
                  style={{
                    marginRight: "8px",
                    color: "black",
                    fontSize: "23px",
                  }}
                >
                  <BsInstagram />
                </a>
                <a
                  href=""
                  style={{
                    marginRight: "8px",
                    color: "black",
                    fontSize: "22px",
                  }}
                >
                  <FaTiktok />
                </a>
                <a
                  href=""
                  style={{
                    marginRight: "8px",
                    color: "black",
                    fontSize: "23px",
                  }}
                >
                  <BsTwitter />
                </a>
                <a
                  href=""
                  style={{
                    marginRight: "8px",
                    color: "black",
                    fontSize: "23px",
                  }}
                >
                  <BsYoutube />
                </a>
              </div>
              <Navbar.Toggle />
              <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                  2022{" "}
                  <a href="#login" className="text-decoration-none">
                    Mushroom.co.id
                  </a>
                </Navbar.Text>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </div>
      </footer>
    </>
  );
};

export default Footeruser;
