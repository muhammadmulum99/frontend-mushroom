import React from "react";
import { useState, useEffect } from "react";
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import florplan from "./florplan.jpg";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Managementfacilities = () => {
  const [modal, setModal] = useState(false);
  const [show, setShow] = useState(false);
  const [keys, setKeys] = useState(0)
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const handleClose = () => setModal(false);
  const handleShow = () => setModal(true);  

  const initialState = {
    id_properti_fasilitas_hotel: "",
    id: "",
    nama_equipment: "",
    kondisi_equipment: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [facility, setFacility] = useState([])
  const [prop, setProp] = useState([])
  const [equipment, setEquipment] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getFacility = async () => {
    const response = await axios.get(`http://localhost:5000/managementfasilitas/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setFacility(decryptedData)
    }    
  }

  const getPropertyFacility = async () => {
    const response = await axios.get(`http://localhost:5000/propertifasilitashotelid/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setProp(decryptedData)
    }    
  }

  const getEquipment = async (x) => {
    const response = await axios.get(`http://localhost:5000/equipmenthotel/${x}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setEquipment(decryptedData)
      setShow(true)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
    getFacility();
  },[data.id_management])

  useEffect(() => {
    if(data.id_properti_hotel=== '') return;
    getPropertyFacility();
  },[data.id_properti_hotel])

  const saveEquipment = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/equipmenthotel", data);
      setData({
        ...data,
        id_properti_fasilitas_hotel: '',
        id: '',
        nama_equipment: '',
        kondisi_equipment: '',
        address: ''
      })
      getFacility();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateEquipment = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/equipmenthotel/"+data.id, data);
      setData({
        ...data,
        id_properti_fasilitas_hotel: '',
        id: '',
        nama_equipment: '',
        kondisi_equipment: '',
        address: ''
      })
      getFacility();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data, 
      id: equipment[key].id, 
      uuid_equipment_hotel: equipment[key].uuid_equipment_hotel, 
      nama_equipment: equipment[key].nama_equipment,
      kondisi_equipment: equipment[key].kondisi_equipment,
      id_properti_fasilitas_hotel: equipment[key].id_properti_fasilitas_hotel    
    })
    setModal(true)
    setShow(false)
  }

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-8">
                      <div class="card">
                        <div class="card-header">
                          <h5>Hotel Floor Plan</h5>
                          <div style={{ float: "right" }}>
                            <Dropdown>
                              <Dropdown.Toggle
                                variant="light"
                                id="dropdown-basic"
                              >
                                Floor
                              </Dropdown.Toggle>
  
                              <Dropdown.Menu>
                                <Dropdown.Item href="#/action-1">
                                  Lantai 1
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-2">
                                  Laintai 2
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-3">
                                  Lantai 3
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                        <div class="card-body">
                          <Card.Img variant="top" src={florplan} />
                        </div>
                      </div>
                    </div>
  
                    {/* test */}
                    <div class="col-sm-4">
                      <div class="card">
                        <div class="card-header">
                          <h5>Facilities</h5>
  
                          <Button
                            style={{
                              float: "right",
                              backgroundColor: "#8F00FF",
                            }}
                            onClick={() => setModal(true)}
                          >
                            Edit
                          </Button>
                        </div>
                        <div class="card-body">
                          {facility.map((item,key) => {
                            return(
                              item.id_master_fasilitas !== '' ?
                              item.id_properti_fasilitas_hotel === '' ?                              
                              <div>
                                <hr align="left" width="350" />
                                <div className="row">
                                  <div className="col-sm-8">
                                    <h5>{item.nama_equipment}: {item.jumlah_equipment === '1' ? 'Open' : 'Closed'}</h5>
                                  </div>
                                  <div className="col-sm-4">
                                    <Button
                                      style={{
                                        float: "right",
                                        backgroundColor: "#8F00FF",
                                      }}
                                      onClick={() => {
                                        setModal(true)
                                      }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </div>                                                                
                              </div>:
                              <div className="contentkiri-promo">
                                <h3>{item.nama_equipment}:</h3>
                                <p>{item.jumlah_equipment}</p>
                                <Button
                                  style={{
                                    float: "right",
                                    backgroundColor: "#8F00FF",
                                  }}
                                  onClick={() => {getEquipment(item.id_properti_fasilitas_hotel)}}
                                >
                                  Edit
                                </Button>
                              </div> : null
                            )
                          })}                                                    
                        </div>
                      </div>
                    </div>
                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={show} onHide={() => setShow(false)} size='xl'>
        <Modal.Header closeButton>
          <Modal.Title>Equipment list</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table>
            <thead>
              <tr>
                <th>#</th>
                <th>ID Equipment</th>
                <th>ID Property Facility Hotel</th>
                <th>Equipment Name</th>
                <th>Equipment Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {equipment.map((item,key) => {
                return(
                  <tr key={key}>
                    <td>{key}</td>
                    <td>{item.uuid_equipment_hotel}</td>
                    <td>{item.id_properti_fasilitas_hotel}</td>
                    <td>{item.nama_equipment}</td>
                    <td>{item.kondisi_equipment === '1' ? 'Active' : item.kondisi_equipment === '2' ? 'Inactive': 'Under Maintenance'}</td>
                    <td>
                      <Button
                        size='sm'
                        style={{
                          backgroundColor: "#8F00FF",
                        }}
                        onClick={() => {
                          setInputFromTable(key)
                        }}
                      >
                        Select
                      </Button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> 
      <Modal show={modal} onHide={handleClose} size='xl'>
        <Modal.Header closeButton>
          <Modal.Title>Add/edit facility</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <Col sm={4}>
              <Form>
                <Row className="mb-2">
                   <Form.Group>
                      <Form.Control type="text" placeholder="ID PROPERTY FACILITY HOTEL" value={data.id_properti_fasilitas_hotel}/>
                   </Form.Group>
                </Row>
                <Row className="mb-2">
                   <Form.Group>
                      <Form.Control type="text" placeholder="EQUIPMENT NAME" name="nama_equipment" value={data.nama_equipment} onChange={handleInputChange}/>
                   </Form.Group>
                </Row>
                <Row className="mb-2">
                  <Form.Group>
                    <Form.Control as="select" value={data.kondisi_equipment} name="kondisi_equipment" onChange={handleInputChange}>
                      <option value="">Equipment Condition</option>
                      <option value="1">Active</option>
                      <option value="2">Inactive</option>
                      <option value="3">Under Maintainance</option>
                    </Form.Control>
                  </Form.Group>
                </Row>
              </Form>
            </Col>
            <Col sm={8}>
              <Table>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID Property Facility Hotel</th>
                    <th>ID Property Hotel</th>
                    <th>Facility Name</th>
                    <th>Facility Status</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {prop.map((item,key) => {
                    return(
                      <tr key={key}>
                        <td>{key}</td>
                        <td>{item.uuid_properti_fasilitas_hotel}</td>
                        <td>{item.id_properti_hotel}</td>
                        <td>{item.nama_master_fasilitas}</td>
                        <td>{item.status_properti_fasilitas_hotel}</td>
                        <td>
                          <Button
                            size='sm'
                            style={{
                              backgroundColor: "#8F00FF",
                            }}
                            onClick={() => {
                              setData({
                                ...data,  
                                id_properti_fasilitas_hotel: prop[key].uuid_properti_fasilitas_hotel    
                              })
                            }}
                          >
                            Select
                          </Button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </Table>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
          <Button variant="success" onClick={saveEquipment}>
            Save
          </Button>
          <Button variant="primary" onClick={updateEquipment}>
            Update
          </Button>
        </Modal.Footer>
      </Modal> 
      {/* [ Main Content ] end */}
    </>
  )
};

export default Managementfacilities;
