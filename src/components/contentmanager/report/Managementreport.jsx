import React from "react";
import { useState, useEffect } from "react";
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import { BsInfoCircle } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";


const Managementreport = () => {
  const [modal, setModal] = useState(false);
  const [keys, setKeys] = useState(0)

  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();

  const handleClose = () => {
    setModal(false)
  }

  const dummy = [
    {
      time: '10:00',
      date: '2022-10-13',
      details: '',
      type: 'Type',     
    },
  ]

  const initialState = {
    id: "",    
    id_management: "",
    id_properti_hotel: "",
    time: '',
    date: '',
    type: '',
    file: ''
  }

  const [data, setData] = useState(initialState)
  const [report, setReport] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getReport = async () => {
    const response = await axios.get(`http://localhost:5000/getreportmanagement/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setReport(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
  },[data.id_management])

  useEffect(() => {
    if(data.id_properti_hotel=== '') return;
    getReport();
  },[data.id_properti_hotel])

  const saveReport = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/reportmanager", data);
      setData(initialState);
      getReport();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateReport = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/reportmanager/"+data.id, data);
      setData(initialState);
      getReport();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data, 
      id: report[key].id, 
      id_properti_hotel: report[key].id_properti_hotel,
      time: report[key].time_report,
      date: report[key].date_report,
      details: report[key].detail_report,
      type: report[key].type_report,
    })
    setModal(true)
  }

  const downloadBase64File = (base64Data) => {
    const linkSource = `data:image/jpeg;base64,${base64Data}`;
    const downloadLink = document.createElement("a");
    downloadLink.href = linkSource;
    downloadLink.download = 'fotohotel.jpeg';
    downloadLink.click();
  }

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        setData({
          ...data,
          file: base64String,
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Report</h5>
                          <Button onClick={() => {
                              setModal(true)
                              setKeys(-1)
                            }} style={{ float: "right" }}>Add</Button>
                        </div>
                        <div class="card-body">
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Time</th>
                                <th>Date</th>

                                <th>Type</th>
                                <th>Details</th>
                              </tr>
                            </thead>
                            <tbody>
                              {report.map((item,key) => {
                                return(
                                  <tr key={key}>
                                    <td>{item.id}</td>
                                    <td>{item.time_report}</td>
                                    <td>{item.date_report}</td>
                                    <td>{item.type_report}</td>
                                    <td>
                                      <Button 
                                      onClick={() => {
                                        setInputFromTable(key)
                                      }}>
                                        Edit
                                      </Button>
                                      <Button onClick={() => downloadBase64File(item.file_report) }>
                                        Download
                                      </Button>
                                    </td>
                                  </tr>
                                )
                              })}
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={modal} onHide={handleClose} size='lg'>
        <Modal.Header closeButton>
          <Modal.Title>Add/edit report</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control type="time" value={data.time} name='time' onChange={handleInputChange}/>
              </Col>
              <Col>
                <Form.Control type="date" placeholder="Upload Floor Layout Picture" value={data.date} name='date' onChange={handleInputChange}/>
              </Col>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Type" value={data.type} name='type' onChange={handleInputChange}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Details" value={data.details} name='details' onChange={handleInputChange}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="file" placeholder="Upload" onChange={uploadFiles}/>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
          <Button variant="success" onClick={saveReport}>
            Save
          </Button>
          <Button variant="primary" onClick={updateReport}>
            Update
          </Button>
        </Modal.Footer>
      </Modal> 
      {/* [ Main Content ] end */}
    </>
  );
};

export default Managementreport;
