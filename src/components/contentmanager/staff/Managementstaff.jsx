import React, { useState, useEffect } from "react";
import { Button, Table, Modal, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Managementstaff = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [staff, setStaff] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/managementstaff/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setStaff(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
    getStaff();
  },[data.id_management])

  const saveStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/createstaffmanagement", data);
      getStaff();
      setData(initialState)
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/updatestaffmanagement/"+data.id, data);
      getStaff();
      setData(initialState);
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data,
      id: staff[key].id,
      nama_staff: staff[key].nama_staff,
      email: staff[key].email,
      phone_number: staff[key].phone_number,
      position: staff[key].position,
      username: staff[key].username,
      password: staff[key].password,
      id_owner: staff[key].id_owner,
      id_management: staff[key].id_management,
      id_properti_hotel: staff[key].id_properti_hotel,
    })
  }

  return (
    <>
      {/* modal */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Staff</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Name</Form.Label>
              <Form.Control name="nama_staff" value={data.nama_staff} onChange={handleInputChange} type="text" placeholder="Enter name" required />
            </Form.Group>
            {/* <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Staff ID</Form.Label>
              <Form.Control name="nama_staff" value={data.nama_staff} type="text" placeholder="Enter Staff ID" required />
            </Form.Group> */}

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Address</Form.Label>
              <Form.Control name="address" value={data.address} onChange={handleInputChange} type="text" placeholder="Enter Adress" required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" value={data.email} onChange={handleInputChange} type="email" placeholder="Enter email" required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                name="phone_number" value={data.phone_number}
                onChange={handleInputChange}
                type="text"
                placeholder="Enter Phone number"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Position</Form.Label>
              <Form.Control name="position" value={data.position} onChange={handleInputChange} type="text" placeholder="Enter Position" required />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control name="password" value={data.password} onChange={handleInputChange} type="password" placeholder="Password" required />
            </Form.Group>
            <div style={{ float: "right" }}>
              <Button variant="primary" onClick={saveStaff}>
                Save
              </Button>
              <Button variant="primary" onClick={updateStaff} >
                Update
              </Button>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      {/* end modal */}
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Staff Data</h5>
                          <div style={{ float: "right" }}>
                            <Button
                              style={{ backgroundColor: "#7F00E2" }}
                              variant="primary"
                              onClick={handleShow}
                            >
                              New{" "}
                            </Button>
                          </div>
                        </div>
                        <div class="card-body">
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Staff ID</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Position</th>
                                <th>Password</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {staff.map((item,key) => {
                                return(
                                  <tr key={key}>
                                    <td>{item.id}</td>
                                    <td>{item.nama_staff}</td>
                                    <td>{item.uuid_staff}</td>
                                    <td></td>
                                    <td>{item.email}</td>
                                    <td>{item.phone_number}</td>
                                    <td>{item.position}</td>
                                    <th>{item.password}</th>
                                    <th>
                                      <Button
                                        style={{ backgroundColor: "#7F00E2" }}
                                        onClick={() => {
                                          setInputFromTable(key);
                                          handleShow();
                                        }}
                                      >
                                        Update
                                      </Button>
                                    </th>
                                  </tr>
                                )
                              })}
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Staff Attendce</h5>
                        </div>
                        <div class="card-body">
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Clock in Time</th>
                                <th>Clock Out Time</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>2022-10-25</td>
                                <td>Staff</td>
                                <td>08:00</td>
                                <td>17:00</td>
                                <td>
                                  <Button
                                    style={{ backgroundColor: "#7F00E2" }}
                                  >
                                    Update
                                  </Button>
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Managementstaff;
