import React, { useState, useEffect } from "react";
import { Row, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Ownerprofile = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
    total_properti: "",
    total_staff: "",
  }

  const [data, setData] = useState(initialState)
  const [hotel, setHotel] = useState([])
  const [facility, setFacility] = useState([])
  const [room, setRoom] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_owner: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/ownerprofileid/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        nama: decryptedData[0].name,
        address: decryptedData[0].alamat_hotel,
        total_properti: decryptedData[0].total_properti,
        total_staff: decryptedData[0].total_staff,
      })
    }    
  }

  const getProp = async () => {
    const response = await axios.get(`http://localhost:5000/ownerproperti/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setHotel(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_owner === '') return;
    getHotel();
    getProp();
  },[data.id_owner])

  const getProfile = async () => {
    const response = await axios.get(`http://localhost:5000/ownerprofile/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setFacility(decryptedData.datafasilitas)
      setRoom(decryptedData.dataroom)
    }    
  }

  useEffect(() => {
    if(data.id_properti_hotel === '') return;    
    getProfile();
  },[data.id_properti_hotel])

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <Row>
                            <h5>Profile</h5>
                          </Row>                          
                        </div>
                        <div class="card-body">
                          <Row>
                            <h6>{data.nama}</h6>
                          </Row>
                          <Row>
                            <p>{data.address}</p>
                          </Row>
                          <Row>
                            <p>Total Property Owned: {data.total_properti}</p>
                          </Row>
                          <Row>
                            <p>Total Staff: {data.total_staff}</p>
                          </Row>
                          <select className="mb-2" name="id_properti_hotel" onChange={handleInputChange}>
                            <option value="">Pilih Properti</option>
                            {hotel.map((item,key) => {
                              return(
                                <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                              )
                            })}
                          </select>
                          {/* <p>Property Name</p>
                          <p>Property Address</p> */}
                          <h6>Facilities</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Name Facility</th>
                                  <th>Status Facilities</th>
                                </tr>
                              </thead>
                              <tbody>
                                {facility.filter((fil) => fil.id_properti_kamar === '' && fil.status_properti_fasilitas_ekstra !== '').map((item,key) => {
                                  return(
                                    <tr>
                                      <td>{key+1}</td>
                                      <td>{item.nama_master_fasilitas}</td>
                                      <td>{item.status_properti_fasilitas_ekstra === '1' ? 'Open' : 'Closed'}</td>
                                    </tr>
                                  )
                                })}
                              </tbody>
                            </Table>
                          </div>

                          <h6>Rooms</h6>
                          {room.map((item,key) => {
                            return(
                              key === 0 ?
                              <div class="row">
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div>
                                {room[key+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key+1].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+1].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key+1].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key+2].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+2].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key+2].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div> :
                              key === room.length - 2 ?
                              null :
                              <div class="row">
                                {room[key*3] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3+1].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3+1].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3+1].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>{room[key*3+2].type_kamar}</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3+2].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3+2].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div>
                            )
                          })}

                          {/* <h6>Staff</h6>
                          <div class="col-sm-6">
                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>Status Staff</th>
                                  <th>Jumlah</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Total Staff</td>
                                  <td>100</td>
                                </tr>
                                <tr>
                                  <td>On Shift</td>
                                  <td>50</td>
                                </tr>
                                <tr>
                                  <td>Off Shift</td>
                                  <td>25</td>
                                </tr>
                                <tr>
                                  <td>Sick Leave</td>
                                  <td>5</td>
                                </tr>
                                <tr>
                                  <td>Vacation</td>
                                  <td>15</td>
                                </tr>
                                <tr>
                                  <td>Other</td>
                                  <td>50</td>
                                </tr>
                              </tbody>
                            </Table>
                          </div>

                          <h6>Last Financial Update : 2022/10/14</h6>
                          <h6>Last Report Update : 2022/10/14</h6> */}
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Ownerprofile;
