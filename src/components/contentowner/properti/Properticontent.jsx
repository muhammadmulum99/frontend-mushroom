import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Properticontent = () => {
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false); 

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);
  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);
  const handleClose4 = () => setShow4(false);
  const handleShow4 = () => setShow4(true);
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    jenis_hotel: '',
    alamat_hotel: '',
    provinsi_hotel: '',
    kecamatan_hotel: '',
    long_lat_hotel: '',
    fasilitas: '',
    foto_hotel: 'x',
    id_properti_kamar: '',
    id_properti_kamar_detail: '',
    type_kamar: '',
    fasilitas_kamar: '',
    rating: '',
    jumlah_kamar: '',
    harga_weekday: '',
    harga_weekend: '',
    foto_kamar: 'x',
    id_master_fasilitas: '',
    status_properti_fasilitas_hotel: '',
    foto_fasilitas_hotel: 'x',
    status_properti_fasilitas_kamar: '',
    foto_fasilitas_kamar: 'x',
    address: "",
    nomor_kamar: '',
    lantai_kamar: '',
    nomor_kamar: '',
    status_kamar_detail: '',
  }

  const [data, setData] = useState(initialState)
  const [hotel, setHotel] = useState([])
  const [facility, setFacility] = useState([])
  const [room, setRoom] = useState([])
  const [manager, setManager] = useState([])
  const [umum, setUmum] = useState([])
  const [masterFacility, setMasterFacility] = useState([])
  const [priv, setPriv] = useState([])
  const [privFac, setPrivFac] = useState([])
  const [masterRoom, setMasterRoom] = useState([])
  const [detail, setDetail] = useState([])
  const [detailHotel, setDetailHotel] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_owner: user.uuid})
    }
  }, [isError, user, navigate]);

  const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/ownerprofileid/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        nama: decryptedData[0].name,
        address: decryptedData[0].alamat_hotel,
        total_properti: decryptedData[0].total_properti,
        total_staff: decryptedData[0].total_staff,
      })
    }    
  }

  const getUmum = async () => {
    const response = await axios.get(`http://localhost:5000/propertifasilitashotelid/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setUmum(decryptedData)
    }    
  }

  const getMasterFacility = async () => {
    const response = await axios.get(`http://localhost:5000/fasilitas`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setMasterFacility(decryptedData)
    }    
  }

  const getPriv = async () => {
    const response = await axios.get(`http://localhost:5000/propertikamarid/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setPriv(decryptedData)
    }    
  }

  const getPrivFac = async () => {
    const response = await axios.get(`http://localhost:5000/propertifasilitaskamardetailid/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setPrivFac(decryptedData)
    }    
  }

  const getMasterRoom = async () => {
    const response = await axios.get(`http://localhost:5000/masterkamar`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setMasterRoom(decryptedData)
    }    
  }


  const getManager = async () => {
    const response = await axios.get(`http://localhost:5000/usermanager`);
    // const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    // const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(response){
      setManager(response.data)
    }    
  }

  const getProp = async () => {
    const response = await axios.get(`http://localhost:5000/ownerproperti/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setHotel(decryptedData)
    }    
  }

  const getDetailHotel = async (x,y) => {
    const response = await axios.get(`http://localhost:5000/propertikamardetailid/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setDetailHotel(decryptedData)
    }    
  }

  const getDetailKamar = async (x,y) => {
    const response = await axios.get(`http://localhost:5000/propertikamardetailid/${x}/${y}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setDetail(decryptedData)
      handleShow4();
    }    
  }

  useEffect(() => {
    if(data.id_owner === '') return;
    getHotel();
    getProp();
    getManager();
  },[data.id_owner])

  const getProfile = async () => {
    const response = await axios.get(`http://localhost:5000/ownerprofile/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setFacility(decryptedData.datafasilitas)
      setRoom(decryptedData.dataroom)
    }    
  }

  useEffect(() => {
    if(data.id_properti_hotel === '') return;    
    getProfile();
    getUmum();
    getMasterFacility();
    getPriv();
    getMasterRoom();
    getPrivFac();
    getDetailHotel();
  },[data.id_properti_hotel])

  const saveProperti = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertihotel", {
        jenis_hotel: data.jenis_hotel,
        alamat_hotel: data.alamat_hotel,
        provinsi_hotel: data.provinsi_hotel,
        kecamatan_hotel: data.kecamatan_hotel,
        long_lat_hotel: data.long_lat_hotel,
        fasilitas: data.fasilitas,
        id_owner: data.id_owner,
        id_management: data.id_management,
        foto_hotel: data.foto_hotel
      });
      setData(initialState)
      getProp();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateProperti = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/propertihotel/"+data.id, {
        jenis_hotel: data.jenis_hotel,
        alamat_hotel: data.alamat_hotel,
        provinsi_hotel: data.provinsi_hotel,
        kecamatan_hotel: data.kecamatan_hotel,
        long_lat_hotel: data.long_lat_hotel,
        fasilitas: data.fasilitas,
        id_owner: data.id_owner,
        id_management: data.id_management,
        foto_hotel: data.foto_hotel
      });
      setData(initialState)
      getProp();
      handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // setData(initialState);
  };

  const saveUmum = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertifasilitashotel", {
        id_properti_hotel: data.id_properti_hotel,
        id_master_fasilitas: data.id_master_fasilitas,
        status_properti_fasilitas_hotel: data.status_properti_fasilitas_hotel,
        foto_fasilitas_hotel: data.foto_fasilitas_hotel
      });
      setData(initialState)
      getProp();
      handleClose1();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateUmum = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/propertifasilitashotel/"+data.id, {
        id_properti_hotel: data.id_properti_hotel,
        id_master_fasilitas: data.id_master_fasilitas,
        status_properti_fasilitas_hotel: data.status_properti_fasilitas_hotel,
        foto_fasilitas_hotel: data.foto_fasilitas_hotel
      });
      setData(initialState)
      getUmum();
      handleClose1();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // setData(initialState);
  };

  const saveKamar = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertikamar", {
        id_properti_hotel: data.id_properti_hotel,
        type_kamar: data.type_kamar,
        fasilitas_kamar: data.fasilitas_kamar,
        rating: data.rating,
        jumlah_kamar: data.jumlah_kamar,
        harga_weekday: data.harga_weekday,
        harga_weekend: data.harga_weekend,
        foto_kamar: data.foto_kamar
      });
      setData(initialState)
      getPriv();
      handleClose2();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateKamar = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/propertikamar/"+data.id, {
        id_properti_hotel: data.id_properti_hotel,
        type_kamar: data.type_kamar,
        fasilitas_kamar: data.fasilitas_kamar,
        rating: data.rating,
        jumlah_kamar: data.jumlah_kamar,
        harga_weekday: data.harga_weekday,
        harga_weekend: data.harga_weekend,
        foto_kamar: data.foto_kamar
      });
      setData(initialState)
      getPriv();
      handleClose2();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // setData(initialState);
  };

  const saveKamarDetail = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertikamardetail", {
        id_properti_hotel: data.id_properti_hotel,
        id_properti_kamar: data.id_properti_kamar,
        nomor_kamar: data.nomor_kamar,
        lantai_kamar: data.lantai_kamar,
        status_kamar_detail: data.status_kamar_detail,
      });
      setData(initialState)
      handleClose4();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateKamarDetail = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/propertikamardetail/"+data.id, {
        id_properti_hotel: data.id_properti_hotel,
        id_properti_kamar: data.id_properti_kamar,
        nomor_kamar: data.nomor_kamar,
        lantai_kamar: data.lantai_kamar,
        status_kamar_detail: data.status_kamar_detail,
      });
      setData(initialState)
      handleClose4();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // setData(initialState);
  };

  const saveKamarFas = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/propertifasilitaskamardetail", {
        id_properti_kamar: data.id_properti_kamar,
        id_properti_kamar_detail: data.id_properti_kamar_detail,
        id_master_fasilitas: data.id_master_fasilitas,
        status_properti_fasilitas_kamar: data.status_properti_fasilitas_kamar,
        foto_fasilitas_kamar: data.foto_fasilitas_kamar,
      });
      setData(initialState)
      handleClose3();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateKamarFas = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/propertifasilitaskamardetail/"+data.id, {
        id_properti_kamar: data.id_properti_kamar,
        id_properti_kamar_detail: data.id_properti_kamar_detail,
        id_master_fasilitas: data.id_master_fasilitas,
        status_properti_fasilitas_kamar: data.status_properti_fasilitas_kamar,
        foto_fasilitas_kamar: data.foto_fasilitas_kamar,
      });
      setData(initialState)
      handleClose3();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
    // setData(initialState);
  };

  const setInputFromTableProperti = (key) => {
    setData({
      ...data, 
      id: hotel[key].id, 
      jenis_hotel: hotel[key].jenis_hotel,
      alamat_hotel: hotel[key].alamat_hotel,
      provinsi_hotel: hotel[key].provinsi_hotel,
      kecamatan_hotel: hotel[key].kecamatan_hotel,
      long_lat_hotel: hotel[key].long_lat_hotel,
      fasilitas: hotel[key].fasilitas, 
      id_owner: hotel[key].id_owner,
      id_management: hotel[key].id_management, 
    })
  }

  const setInputFromTableUmum = (key) => {
    setData({
      ...data, 
      id: umum[key].id, 
      id_master_fasilitas: umum[key].id_master_fasilitas, 
      status_properti_fasilitas_hotel: umum[key].status_properti_fasilitas_hotel, 
    })
  }

  const setInputFromTableKamar = (key) => {
    setData({
      ...data, 
      id: priv[key].id, 
      id_properti_kamar: priv[key].id_properti_kamar, 
      type_kamar: priv[key].type_kamar, 
      fasilitas_kamar: priv[key].fasilitas_kamar, 
      rating: priv[key].rating, 
      jumlah_kamar: priv[key].jumlah_kamar, 
      harga_weekday: priv[key].harga_weekday, 
      harga_weekend: priv[key].harga_weekend, 
    })
  }

  const setInputFromTableKamarDetail = (key) => {
    setData({
      ...data, 
      id: detail[key].id, 
      id_properti_kamar: detail[key].id_properti_kamar, 
      nomor_kamar: detail[key].nomor_kamar, 
      lantai_kamar: detail[key].lantai_kamar, 
      status_kamar_detail: detail[key].status_kamar_detail, 
    })
  }

  const setInputFromTableKamarFas = (key) => {
    setData({
      ...data, 
      id: privFac[key].id, 
      id_properti_kamar_detail: privFac[key].id_properti_kamar_detail,
      id_properti_kamar: privFac[key].id_properti_kamar, 
      id_master_fasilitas: privFac[key].id_master_fasilitas, 
      status_properti_fasilitas_kamar: privFac[key].status_properti_fasilitas_kamar, 
    })
  }

  // const downloadBase64File = (base64Data) => {
  //   const linkSource = `data:image/jpeg;base64,${base64Data}`;
  //   const downloadLink = document.createElement("a");
  //   downloadLink.href = linkSource;
  //   downloadLink.download = 'fotohotel.jpeg';
  //   downloadLink.click();
  // }

  const uploadFiles = (e) => {
    var f = e.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(x) {
        var binaryData = x.target.result;
        //Converting Binary Data to base 64
        var base64String = window.btoa(binaryData);
        //showing file converted to base64
        // document.getElementById('base64').value = base64String;
        console.log(e.target.name)
        setData({
          ...data,
          [e.target.name]: base64String
        })
        // alert('File converted to base64 successfuly!\nCheck in Textarea');
      };
    })(f);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(f);
  }

  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <Button size="sm" variant="primary" onClick={handleShow}>
                            Add Property
                          </Button>

                          <Modal show={show} onHide={handleClose} size='xl'>
                            <Modal.Header closeButton>
                              <Modal.Title>Add Property</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Row>
                                <Col sm={3}>
                                  <Form>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Property Name"
                                        name="jenis_hotel"
                                        onChange={handleInputChange}
                                        value={data.jenis_hotel}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        as="textarea"
                                        placeholder="Property Address"
                                        name="alamat_hotel"
                                        onChange={handleInputChange}
                                        value={data.alamat_hotel}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Province"
                                        name="provinsi_hotel"
                                        onChange={handleInputChange}
                                        value={data.provinsi_hotel}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Sub-district"
                                        name="kecamatan_hotel"
                                        onChange={handleInputChange}
                                        value={data.kecamatan_hotel}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Long/lat"
                                        name="long_lat_hotel"
                                        onChange={handleInputChange}
                                        value={data.long_lat_hotel}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="ID Management"
                                        name="id_management"
                                        onChange={handleInputChange}
                                        value={data.id_management}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Facilities"
                                        name="fasilitas"
                                        onChange={handleInputChange}
                                        value={data.fasilitas}
                                      />
                                    </Form.Group>                                
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="file"
                                        placeholder="Property Name"
                                        name="foto_hotel"
                                        onChange={uploadFiles}
                                      />
                                    </Form.Group>
                                    {/* <Button variant="primary" type="submit">
                                      Submit
                                    </Button> */}
                                  </Form>
                                </Col>
                                <Col sm={9}>
                                  <Row>
                                    <h4>List of Properties</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Hotel Name</th>
                                          <th>ID Manangement</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {hotel.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.id}</td>
                                              <td>{item.jenis_hotel}</td>
                                              <td>{item.id_management}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setInputFromTableProperti(key)}>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                  <Row>
                                    <h4>List of Managers</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>Name</th>
                                          <th>Email</th>
                                          <th>Role</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {manager.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.name}</td>
                                              <td>{item.email}</td>
                                              <td>{item.role}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setData({ ...data, id_management: item.uuid })}>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                </Col>
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>                              
                              <Button variant="success" onClick={saveProperti}>
                                Save
                              </Button>
                              <Button variant="primary" onClick={updateProperti}>
                                Update
                              </Button>
                              <Button variant="danger" onClick={handleClose}>
                                Close
                              </Button>
                            </Modal.Footer>
                          </Modal>
                          <Modal show={show1} onHide={handleClose1} size='xl'>
                            <Modal.Header closeButton>
                              <Modal.Title>Add/edit facility</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Row>
                                <Col sm={3}>
                                  <Form>                                
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Facility"
                                        name="id_master_fasilitas"
                                        value={data.id_master_fasilitas}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>  
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Status"
                                        name="status_properti_fasilitas_hotel"
                                        value={data.status_properti_fasilitas_hotel}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>                               
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="file"
                                        placeholder="Property Name"
                                        name="foto_fasilitas_hotel"
                                        onChange={uploadFiles}
                                      />
                                    </Form.Group>
                                    {/* <Button variant="primary" type="submit">
                                      Submit
                                    </Button> */}
                                  </Form>
                                </Col>
                                <Col sm={9}>
                                  <Row>
                                    <h4>List of facilities owned by property</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Facility Name</th>
                                          <th>Status</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {umum.map((item,key) => {
                                          return(
                                            <tr key={key}>
                                              <td>{item.uuid_properti_fasilitas_hotel}</td>
                                              <td>{item.nama_master_fasilitas}</td>
                                              <td>{item.status_properti_fasilitas_hotel === '1' ? 'Open' : 'Closed'}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setInputFromTableUmum(key) }>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                  <Row>
                                    <h4>List of facilites</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Facility Name</th>
                                          <th>Type</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {masterFacility.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.uuid_master_fasilitas}</td>
                                              <td>{item.nama_master_fasilitas}</td>
                                              <td>{item.jenis_master_fasilitas}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setData({...data, id_master_fasilitas: item.uuid_master_fasilitas}) }>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                </Col>
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>                              
                              <Button variant="success" onClick={saveUmum}>
                                Save
                              </Button>
                              <Button variant="primary" onClick={updateUmum}>
                                Update
                              </Button>
                              <Button variant="danger" onClick={handleClose}>
                                Close
                              </Button>
                            </Modal.Footer>
                          </Modal>
                          <Modal show={show3} onHide={handleClose3} size='xl'>
                            <Modal.Header closeButton>
                              <Modal.Title>Add/edit room facility</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Row>
                                <Col sm={3}>
                                  <Form>                              
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Facility"
                                        name="id_master_fasilitas"
                                        value={data.id_master_fasilitas}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>  
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Status"
                                        name="status_properti_fasilitas_kamar"
                                        value={data.status_properti_fasilitas_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>                               
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="file"
                                        placeholder="Property Name"
                                        name="foto_fasilitas_kamar"
                                        onChange={uploadFiles}
                                      />
                                    </Form.Group>
                                    {/* <Button variant="primary" type="submit">
                                      Submit
                                    </Button> */}
                                  </Form>
                                </Col>
                                <Col sm={9}> 
                                  <Row>
                                    <h4>List of rooms provided by property</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Room Number</th>
                                          <th>Floor Number</th>
                                          <th>Status</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {detailHotel.map((item,key) => {
                                          return(
                                            <tr key={key}>
                                              <td>{item.id}</td>
                                              <td>{item.nomor_kamar}</td>
                                              <td>{item.lantai_kamar}</td>
                                              <td>{item.status_kamar_detail === '1' || 'x' ? 'Open' : 'Closed'}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setData({ ...data, id_properti_kamar_detail: item.uuid_properti_fasilitas_kamar_detail}) }>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>                                 
                                  <Row>
                                    <h4>List of facilites</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Facility Name</th>
                                          <th>Type</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {masterFacility.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.uuid_master_fasilitas}</td>
                                              <td>{item.nama_master_fasilitas}</td>
                                              <td>{item.jenis_master_fasilitas}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setData({ ...data, id_master_fasilitas: item.uuid_master_fasilitas}) }>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                </Col>                                
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>                              
                              <Button variant="success" onClick={saveKamarFas}>
                                Save
                              </Button>
                              <Button variant="primary" onClick={updateKamarFas}>
                                Update
                              </Button>
                              <Button variant="danger" onClick={handleClose}>
                                Close
                              </Button>
                            </Modal.Footer>
                          </Modal>
                          <Modal show={show2} onHide={handleClose2} size='xl'>
                            <Modal.Header closeButton>
                              <Modal.Title>Add/edit room</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Row>
                                <Col sm={3}>
                                  <Form>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Room name"
                                        name="type_kamar"
                                        value={data.type_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        as="textarea"
                                        placeholder="Facility"
                                        name="fasilitas_kamar"
                                        value={data.fasilitas_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Rating"
                                        name="rating"
                                        value={data.rating}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Jumlah Kamar"
                                        name="jumlah_kamar"
                                        value={data.jumlah_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Harga weekday"
                                        name="harga_weekday"
                                        value={data.harga_weekday}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Harga weekend"
                                        name="harga_weekend"
                                        value={data.harga_weekend}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>                            
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="file"
                                        placeholder="Property Name"
                                        name="foto_kamar"
                                        onChange={uploadFiles}
                                      />
                                    </Form.Group>
                                    {/* <Button variant="primary" type="submit">
                                      Submit
                                    </Button> */}
                                  </Form>
                                </Col>
                                <Col sm={9}>
                                  <Row>
                                    <h4>List of rooms provided by property</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Room Name</th>
                                          <th>Total of rooms</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {priv.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.uuid_properti_kamar}</td>
                                              <td>{item.type_kamar}</td>
                                              <td>{item.jumlah_kamar}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setInputFromTableKamar(key)}>
                                                  Select
                                                </Button>
                                                <Button size="sm" variant="primary" onClick={() => getDetailKamar(item.id_properti_hotel,item.uuid_properti_kamar)}>
                                                  Open
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>
                                  <Row>
                                    <h4>List of rooms</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID</th>
                                          <th>Type</th>
                                          <th>Room Name</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {masterRoom.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.uuid_master_kamar}</td>
                                              <td>{item.type_kamar}</td>
                                              <td>{item.nama_kamar}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setData({...data, type_kamar: item.type_kamar })}>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>                                  
                                </Col>
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>                              
                              <Button variant="success" onClick={saveKamar}>
                                Save
                              </Button>
                              <Button variant="primary" onClick={updateKamar}>
                                Update
                              </Button>
                              <Button variant="danger" onClick={handleClose}>
                                Close
                              </Button>
                            </Modal.Footer>
                          </Modal>
                          <Modal show={show4} onHide={handleClose4} size='xl'>
                            <Modal.Header closeButton>
                              <Modal.Title>Add/edit room detail</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                              <Row>
                                <Col sm={3}>
                                  <Form>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Room number"
                                        name="nomor_kamar"
                                        value={data.nomor_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        as="textarea"
                                        placeholder="Floor number"
                                        name="lantai_kamar"
                                        value={data.lantai_kamar}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>
                                    <Form.Group
                                      className="mb-2"
                                      controlId="formBasicEmail"
                                    >
                                      <Form.Control
                                        type="text"
                                        placeholder="Status"
                                        name="status_kamar_detail"
                                        value={data.status_kamar_detail}
                                        onChange={handleInputChange}
                                      />
                                    </Form.Group>                                   
                                    {/* <Button variant="primary" type="submit">
                                      Submit
                                    </Button> */}
                                  </Form>
                                </Col>
                                <Col sm={9}>
                                  <Row>
                                    <h4>List of rooms provided by property</h4>
                                    <Table striped bordered hover>
                                      <thead>
                                        <tr>
                                          <th>ID Hotel</th>
                                          <th>ID Room</th>
                                          <th>Room Number</th>
                                          <th>Floor Number</th>
                                          <th>Status</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {detail.map((item,key) => {
                                          return(
                                            <tr>
                                              <td>{item.id_properti_hotel}</td>
                                              <td>{item.id_properti_kamar}</td>
                                              <td>{item.nomor_kamar}</td>
                                              <td>{item.lantai_kamar}</td>
                                              <td>{item.status_kamar_detail}</td>
                                              <td>
                                                <Button size="sm" variant="primary" onClick={() => setInputFromTableKamarDetail(key)}>
                                                  Select
                                                </Button>
                                              </td>
                                            </tr>
                                          )
                                        })}
                                      </tbody>
                                    </Table>
                                  </Row>                                                                   
                                </Col>
                              </Row>
                            </Modal.Body>
                            <Modal.Footer>                              
                              <Button variant="success" onClick={saveKamarDetail}>
                                Save
                              </Button>
                              <Button variant="primary" onClick={updateKamarDetail}>
                                Update
                              </Button>
                              <Button variant="danger" onClick={handleClose}>
                                Close
                              </Button>
                            </Modal.Footer>
                          </Modal>
                        </div>
                        <div class="card">
                          <div class="card-body">                            
                            <Row className="mb-2" >
                              <Col xs='auto'>
                                <select className="mb-2" name="id_properti_hotel" onChange={handleInputChange}>
                                  <option value="">Pilih Properti</option>
                                  {hotel.map((item,key) => {
                                    return(
                                      <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                                    )
                                  })}
                                </select>
                              </Col>
                              <Col>
                                
                              </Col>
                            </Row>
                            {/* <p>Property Name</p>
                            <p>Property Address</p> */}                            
                            <Row className="mb-2" >
                              <Col xs='auto'>
                                <h4>Facilities</h4>
                              </Col>
                              <Col>
                                <Button size="sm" variant="primary" onClick={handleShow1}>
                                  Add
                                </Button>
                              </Col>
                            </Row>
                            <div class="col-sm-6">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Name Facility</th>
                                    <th>Status Facilities</th>
                                    <th>
                                      
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {facility.filter((fil) => fil.id_properti_kamar === '' && fil.status_properti_fasilitas_ekstra !== '').map((item,key) => {
                                    return(
                                      <tr>
                                        <td>{key+1}</td>
                                        <td>{item.nama_master_fasilitas}</td>
                                        <td>{item.status_properti_fasilitas_ekstra === '1' || 'x' ? 'Open' : 'Closed'}</td>
                                        <td>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </td>
                                      </tr>
                                    )
                                  })}
                                </tbody>
                              </Table>
                            </div>
                                  
                            <Row className="mb-2" >
                              <Col xs='auto'>
                                <h4>Rooms</h4>
                              </Col>
                              <Col>
                                <Button size="sm" variant="primary" onClick={handleShow2}>
                                  Add
                                </Button>
                              </Col>
                            </Row>
                            {room.map((item,key) => {
                            return(
                              key === 0 ?
                              <div class="row">
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div>
                                {room[key+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                  <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key+1].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+1].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key+1].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key+2].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key+2].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key+2].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div> :
                              key === room.length - 2 ?
                              null :
                              <div class="row">
                                {room[key*3] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key*3].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+1] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key*3+1].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3+1].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3+1].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                                {room[key*3+2] ?
                                <div class="col-sm-4">
                                  <Table striped bordered hover>
                                    <thead>
                                      <tr>
                                        <th colSpan={2}>{room[key*3+2].type_kamar}</th>                                        
                                        <th>
                                          <Button size="sm" variant="primary" onClick={handleShow}>
                                            Edit
                                          </Button>
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>1</td>
                                        <td>Total Rooms</td>
                                        <td>{room[key*3+2].jumlah_kamar}</td>
                                      </tr>
                                      <tr>
                                        <td>2</td>
                                        <td>Total Under Maintenance</td>
                                        <td>{room[key*3+2].total_under_maintainence}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </div> :
                                <div class="col-sm-4"></div>}
                              </div>
                            )
                          })}
                            <Row className="mb-2" >
                              <Col xs='auto'>
                                <h4>Room Facilities</h4>
                              </Col>
                              <Col>
                                <Button size="sm" variant="primary" onClick={handleShow3}>
                                  Add
                                </Button>
                              </Col>
                            </Row>
                            <div class="col-sm-6">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>Room Number</th>
                                    <th>Facility Name</th>
                                    <th>Status</th>
                                    <th>
                                      
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {privFac.map((item,key) => {
                                    return(
                                      <tr key={key}>
                                        <td>{item.uuid_properti_fasilitas_kamar_detail}</td>
                                        <td>{item.id_properti_kamar}</td>
                                        <td>{item.id_properti_kamar_detail}</td>
                                        <td>{item.nama_master_fasilitas}</td>
                                        <td>{item.status_properti_fasilitas_kamar === '1' || 'x' ? 'Open' : 'Closed'}</td>
                                        <td>
                                          <Button size="sm" variant="primary" onClick={() => {
                                            setInputFromTableKamarFas(key)
                                            handleShow3()
                                          }}>
                                            Edit
                                          </Button>
                                        </td>
                                      </tr>
                                    )
                                  })}
                                </tbody>
                              </Table>
                            </div>
                            {/* <h6>Staff</h6>
                            <div class="col-sm-6">
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>Status Staff</th>
                                    <th>Jumlah</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Total Staff</td>
                                    <td>100</td>
                                  </tr>
                                  <tr>
                                    <td>On Shift</td>
                                    <td>50</td>
                                  </tr>
                                  <tr>
                                    <td>Off Shift</td>
                                    <td>25</td>
                                  </tr>
                                  <tr>
                                    <td>Sick Leave</td>
                                    <td>5</td>
                                  </tr>
                                  <tr>
                                    <td>Vacation</td>
                                    <td>15</td>
                                  </tr>
                                  <tr>
                                    <td>Other</td>
                                    <td>50</td>
                                  </tr>
                                </tbody>
                              </Table>
                            </div>

                            <h6>Last Financial Update : 2022/10/14</h6>
                            <h6>Last Report Update : 2022/10/14</h6> */}
                          </div>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Properticontent;
