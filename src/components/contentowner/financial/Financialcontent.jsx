import React, { useState, useEffect } from "react";
import CanvasJSReact from './canvasjs.react';
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


const Financialcontent = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [finance, setFinance] = useState([])
  const [dp, setDp] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_owner: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/financialid/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setFinance([...decryptedData])
    }    
  }

  const iterateFinance = () => {
    let temp = []
    let temp1 = []
      for(let j = 0; j <= finance.length - 1; j++){
        if(j === finance.length -1){
          temp.push({
            x: new Date(finance[j].date),
            y: parseFloat(finance[j].income)
          });
          temp1.push({
            name: finance[j].id_properti_hotel,
            type: "spline",
            yValueFormatString: "#0.## °C",
            showInLegend: true,
            dataPoints: temp
          })
          setDp(temp1)
          temp = []
        }      
        else if(finance[j].id_properti_hotel === finance[j+1].id_properti_hotel){
          temp.push({
            x: new Date(finance[j].date),
            y: parseFloat(finance[j].income)
          });
        }
        else{
          temp.push({
            x: new Date(finance[j].date),
            y: parseFloat(finance[j].income)
          });
          temp1.push({
            name: finance[j].id_properti_hotel,
            type: "spline",
            yValueFormatString: "#0.## °C",
            showInLegend: true,
            dataPoints: temp
          })
          temp = []
        }
      }      
  }

  useEffect(() => {
    if(data.id_owner === '') return;
    getHotel();
  },[data.id_owner])

  useEffect(() => {
    if(finance.length === 0) return;
    iterateFinance();
  },[finance])

  // const saveStaff = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await axios.post("http://localhost:5000/createstaffmanagement", data);
  //     getStaff();
  //     setData(initialState)
  //     // handleClose();
  //   } catch (error) {
  //     if (error.response) {
  //       alert(error.response.data.msg);
  //     }
  //   }
  // };

  // const updateStaff = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await axios.patch("http://localhost:5000/updatestaffmanagement/"+data.id, data);
  //     getStaff();
  //     setData(initialState);
  //     // handleClose();
  //   } catch (error) {
  //     if (error.response) {
  //       alert(error.response.data.msg);
  //     }
  //   }
  // };

  // const setInputFromTable = (key) => {
  //   setData({
  //     ...data,
  //     id: staff[key].id,
  //     nama_staff: staff[key].nama_staff,
  //     email: staff[key].email,
  //     phone_number: staff[key].phone_number,
  //     position: staff[key].position,
  //     username: staff[key].username,
  //     password: staff[key].password,
  //     id_owner: staff[key].id_owner,
  //     id_management: staff[key].id_management,
  //     id_properti_hotel: staff[key].id_properti_hotel,
  //   })
  // }

  const dummy = [
    {
      time: '10:00',
      date: '2022-10-13',
      income: '2000000',
      costs: '100000',
      profit: '3000000'
    },
  ]

  const options = {
    animationEnabled: true,
    title:{
      text: "Income of Property"
    },
    axisX: {
      valueFormatString: "DD/MM/YYYY"
    },
    axisY: {
      title: "Income",
    },
    toolTip: {
      shared: "true"
    },
    legend:{
      cursor:"pointer",
      // itemclick : toggleDataSeries
    },
   data: dp
  }
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">test</div>
                        <div class="card-body">
                          <CanvasJSChart options = {options}
                            /* onRef={ref => this.chart = ref} */
                          />
                        </div>
                        <div class="card-body">
                          <Row>
                            <Col md={8}>
                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Time</th>
                                    <th>Date</th>
                                    <th>Income</th>
                                    <th>Conts</th>
                                    <th>Profit</th>
                                    {/* <th>Details</th> */}
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                  {finance.map((item,key) => {
                                    return(
                                      <tr>
                                        <td>{key}</td>
                                        <td>{item.time}</td>
                                        <td>{item.date}</td>
                                        <td>{item.income}</td>
                                        <td>{item.costs}</td>
                                        <td>{item.profit}</td>
                                        {/* <td>
                                          <Button 
                                          onClick={() => {
                                            setModal(true)
                                            setKeys(key)
                                          }}>
                                            Edit
                                          </Button>
                                          <Button>
                                            Download
                                          </Button>
                                        </td> */}
                                      </tr>
                                    )
                                  })}
                                </tbody>
                              </Table>
                            </Col>
                            <Col md={4}>
                                <Row>
                                  Total Overview
                                </Row>
                                <Row>
                                  Total Income {finance.reduce((x,y) => {return x + parseFloat(y.income)}, 0)}
                                </Row>
                                <Row>
                                  Total Costs {finance.reduce((x,y) => {return x + parseFloat(y.costs)}, 0)}
                                </Row>
                                <Row>
                                  Total Profit {finance.reduce((x,y) => {return x + parseFloat(y.profit)}, 0)}
                                </Row>
                                <Row>
                                  Total Overall {finance.reduce((x,y) => {return x + parseFloat(y.income) + parseFloat(y.profit) + parseFloat(y.costs)}, 0)}
                                </Row>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Financialcontent;
