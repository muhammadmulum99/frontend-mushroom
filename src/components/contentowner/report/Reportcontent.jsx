import React, { useState, useEffect } from "react";
import { Button, Table, Dropdown, Image, Card, Modal, Form, Row, Col } from "react-bootstrap";
import { BsInfoCircle } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Reportcontent = () => {
  const [modal, setModal] = useState(false);
  const [keys, setKeys] = useState(0)

  const handleClose = () => setModal(false)  

  const dummy = [
    {
      time: '10:00',
      date: '2022-10-13',
      details: '',
      type: 'Type',     
    },
  ]
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)  
  const [hotel, setHotel] = useState([])
  const [report, setReport] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_owner: user.uuid})
    }
  }, [isError, user, navigate]);

  //  const getHotel = async () => {
  //   const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_owner}`);
  //   const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
  //   const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
  //   if(decryptedData){
  //     setData({
  //       ...data,
  //       id_properti_hotel: decryptedData[0].uuid_properti_hotel
  //     })
  //   }    
  // }

  const getProp = async () => {
    const response = await axios.get(`http://localhost:5000/ownerproperti/${data.id_owner}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setHotel(decryptedData)
    }    
  }

  const getReport = async () => {
    const response = await axios.get(`http://localhost:5000/getreportmanagement/${data.id_properti_hotel}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setReport(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_owner === '') return;
    getProp();
  },[data.id_owner])

  useEffect(() => {
    if(data.id_properti_hotel === '') return;
    getReport();
  },[data.id_properti_hotel])

  // const saveStaff = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await axios.post("http://localhost:5000/createstaffmanagement", data);
  //     getStaff();
  //     setData(initialState)
  //     // handleClose();
  //   } catch (error) {
  //     if (error.response) {
  //       alert(error.response.data.msg);
  //     }
  //   }
  // };

  // const updateStaff = async (e) => {
  //   e.preventDefault();
  //   try {
  //     await axios.patch("http://localhost:5000/updatestaffmanagement/"+data.id, data);
  //     getStaff();
  //     setData(initialState);
  //     // handleClose();
  //   } catch (error) {
  //     if (error.response) {
  //       alert(error.response.data.msg);
  //     }
  //   }
  // };


  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <h5>Report</h5>
                          <select className="mb-2" name="id_properti_hotel" onChange={handleInputChange}>
                            <option value="">Pilih Properti</option>
                            {hotel.map((item,key) => {
                              return(
                                <option key={key} value={item.uuid_properti_hotel}>{item.jenis_hotel}</option>
                              )
                            })}
                          </select>
                          {/* <Button onClick={() => {
                              setModal(true)
                              setKeys(-1)
                            }} style={{ float: "right" }}>Add</Button> */}
                        </div>
                        <div class="card-body">
                          <Table striped bordered hover>
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Time</th>
                                <th>Date</th>                                
                                <th>Type</th>
                                <th>Detail</th>
                                {/* <th>Details</th> */}
                              </tr>
                            </thead>
                            <tbody>
                              {report.map((item,key) => {
                                return(
                                  <tr>
                                    <td>{key}</td>
                                    <td>{item.time_report}</td>
                                    <td>{item.date_report}</td>
                                    <td>{item.type_report}</td>
                                    <td>{item.detail_report}</td>
                                    <td>{item.file_report}</td>
                                    {/* <td>
                                      <Button 
                                      onClick={() => {
                                        setModal(true)
                                        setKeys(key)
                                      }}>
                                        Edit
                                      </Button>
                                      <Button>
                                        Download
                                      </Button>
                                    </td> */}
                                  </tr>
                                )
                              })}
                            </tbody>
                          </Table>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal show={modal} onHide={handleClose} size='lg'>
        <Modal.Header closeButton>
          <Modal.Title>Add/edit report</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
          <Row className="mb-2">
              <Col xs='auto'>
                <Form.Control type="time" value={keys === -1 ? '' : dummy[keys].time}>
                </Form.Control>
              </Col>
              <Col>
                <Form.Control type="date" placeholder="Upload Floor Layout Picture" value={keys === -1 ? '' : dummy[keys].date}/>
              </Col>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Type" value={keys === -1 ? '' : dummy[keys].type}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="text" placeholder="Details" value={keys === -1 ? '' : dummy[keys].details}/>
            </Row>
            <Row className="mb-2">
              <Form.Control type="file" placeholder="Upload"/>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> 
      {/* [ Main Content ] end */}
    </>
  );
};

export default Reportcontent;
