import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col, Modal, Table } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getMe } from "../../../features/authSlice";
import cryptoJs from "crypto-js";

const Settingcontent = () => {
  const dispatch = useDispatch();
  const { isError, user } = useSelector((state) => state.auth);
  const navigate = useNavigate();
  const initialState = {
    // uuid_staff: "",
    id: "",
    nama_staff: "",
    email: "",
    phone_number: "",
    position: "",
    username: "",
    password: "",
    id_owner: "",
    id_management: "",
    id_properti_hotel: "",
    address: "",
  }

  const [data, setData] = useState(initialState)
  const [staff, setStaff] = useState([])

  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  
  useEffect(() => {
    dispatch(getMe());
  }, [dispatch]);

  useEffect(() => {
    if (isError) {
      // navigate("/");
    }
    if(user){
      setData({...data, id_management: user.uuid})
    }
  }, [isError, user, navigate]);

   const getHotel = async () => {
    const response = await axios.get(`http://localhost:5000/managementhotel/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setData({
        ...data,
        id_properti_hotel: decryptedData[0].uuid_properti_hotel
      })
    }    
  }

  const getStaff = async () => {
    const response = await axios.get(`http://localhost:5000/managementstaff/${data.id_management}`);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));    
    if(decryptedData){
      setStaff(decryptedData)
    }    
  }

  useEffect(() => {
    if(data.id_management === '') return;
    getHotel();
    getStaff();
  },[data.id_management])

  const saveStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/createstaffmanagement", data);
      getStaff();
      setData(initialState)
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const updateStaff = async (e) => {
    e.preventDefault();
    try {
      await axios.patch("http://localhost:5000/users/"+data.id, data);
      getStaff();
      setData(initialState);
      // handleClose();
    } catch (error) {
      if (error.response) {
        alert(error.response.data.msg);
      }
    }
  };

  const setInputFromTable = (key) => {
    setData({
      ...data,
      id: staff[key].id,
      nama_staff: staff[key].nama_staff,
      email: staff[key].email,
      phone_number: staff[key].phone_number,
      position: staff[key].position,
      username: staff[key].username,
      password: staff[key].password,
      id_owner: staff[key].id_owner,
      id_management: staff[key].id_management,
      id_properti_hotel: staff[key].id_properti_hotel,
    })
  }
  return (
    <>
      {/* [ Main Content ] start */}
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              {/* [ breadcrumb ] start */}
              <div class="page-header">
                <div class="page-block">
                  <div class="row align-items-center">
                    <div class="col-md-12">
                      <div class="page-header-title">
                        <h5 class="m-b-10">Typography</h5>
                      </div>
                      <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">
                            <i class="feather icon-home"></i>
                          </a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Basic Componants</a>
                        </li>
                        <li class="breadcrumb-item">
                          <a href="javascript:">Typography</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              {/* [ breadcrumb ] end */}
              <div class="main-body">
                <div class="page-wrapper">
                  {/* [ Main Content ] start */}
                  <div class="row">
                    {/* [ Typography ] start */}
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">Setting</div>
                        <div class="card-body">
                          <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Name</Form.Label>
                              <Form.Control type="text" placeholder="Full Name" name="name" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Username</Form.Label>
                              <Form.Control type="text" placeholder="Full Name" name="username" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Password</Form.Label>
                              <Form.Control type="password" placeholder="Full Name" name="username" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Form.Label>Email</Form.Label>
                              <Form.Control type="email" placeholder="Enter email" name="email" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                              <Form.Label>Phone Number</Form.Label>
                              <Form.Control type="text" placeholder="Phone Number" name="phone_number"/>
                            </Form.Group>                            
                            <Button
                              variant="outline-warning"
                              style={{
                                float: "right",
                                height: "40px",
                                width: "100px",
                                marginTop: "20px",
                              }}
                             onClick={updateStaff}
                            >
                              Save
                            </Button>{" "}
                          </Form>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">Notification</div>
                        <div class="card-body">
                          <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                              <Row>
                                <Col md={1}>
                                  <Form.Label>Property A</Form.Label>
                                </Col>
                                <Col md={11}>
                                  <Form.Check 
                                    type='checkbox'
                                    id='default-checkbox'
                                    label='Financial Report Notification'
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col md={1}>
                                  <Form.Label> </Form.Label>
                                </Col>
                                <Col md={11}>
                                  <Form.Check 
                                    type='checkbox'
                                    id='default-checkbox'
                                    label='Report Notification'
                                  />
                                </Col>
                              </Row>
                              <Row>
                                <Col xs='auto'>
                                  <Form.Label>Notification Timeframe at </Form.Label>
                                </Col>
                                <Col xs='auto'>
                                  <Form.Control type="time" placeholder="Full Name" name="name" /> 
                                </Col>
                                <Col xs='auto'>
                                  <Form.Label> every </Form.Label>
                                </Col>
                                <Col md={1}>
                                  <Form.Control type="date" placeholder="Full Name" name="name" />
                                </Col>
                              </Row>
                            </Form.Group>                           
                            <Button
                              variant="outline-warning"
                              style={{
                                float: "right",
                                height: "40px",
                                width: "100px",
                                marginTop: "20px",
                              }}
                              // href="/fo-payment"
                              // onClick={() => {localStorage.setItem("fo", JSON.stringify(input));}}
                            >
                              Apply
                            </Button>{" "}
                          </Form>
                        </div>
                      </div>
                    </div>

                    {/* [ Typography ] end */}
                  </div>
                  {/* [ Main Content ] end */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* [ Main Content ] end */}
    </>
  );
};

export default Settingcontent;
