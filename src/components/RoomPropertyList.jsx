import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import cryptoJs from "crypto-js";

const Banklist = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const response = await axios.get("http://localhost:5000/propertikamar");
    console.log(response);
    const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
    const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
    setUsers(decryptedData);
  };

  const deleteUser = async (userId) => {
    await axios.delete(`http://localhost:5000/propertikamar/${userId}`);
    getUsers();
  };

  const downloadBase64File = (base64Data, x) => {
    const linkSource = `data:image/jpeg;base64,${base64Data}`;
    const downloadLink = document.createElement("a");
    downloadLink.href = linkSource;
    downloadLink.download = x+'.jpeg';
    downloadLink.click();
  }

  return (
    <div>
      <h1 className="title">Room</h1>
      <h2 className="subtitle">List of Room</h2>
      <Link to="/roomproperty/add" className="button is-primary mb-2">
        Add New
      </Link>
      <table className="table is-striped is-fullwidth">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Hotel Property</th>
            <th>ID Room Property</th>
            <th>Room Type</th>
            <th>Room Facility</th>
            <th>Rating</th>
            <th>Number of rooms</th>
            <th>Weekend Price</th>
            <th>Weekday Price</th>
            <th>Room Photo</th>

            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.uuid}>
              <td>{index + 1}</td>
              <td>{user.id_properti_hotel}</td>
              <td>{user.uuid_properti_kamar}</td>
              <td>{user.type_kamar}</td>
              <td>{user.fasilitas_kamar}</td>
              <td>{user.rating}</td>
              <td>{user.jumlah_kamar}</td>
              <td>{user.harga_weekday}</td>
              <td>{user.harga_weekend}</td>
              <td><button onClick={() => downloadBase64File(user.foto_kamar, user.type_kamar)}>Download Photo</button></td>
              <td>
                <Link
                  to={`/roomproperty/edit/${user.uuid_properti_kamar}`}
                  className="button is-small is-info"
                >
                  Edit
                </Link>
                <button
                  onClick={() => deleteUser(user.uuid_properti_kamar)}
                  className="button is-small is-danger"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Banklist;
