import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import cryptoJs from "crypto-js";

const FormAddBank = () => {
  const initialState = {
    id_master_bank: '',
    nama_master_pembayaran: '',
  }
  const [data, setData] = useState(initialState);
  const handleInputChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })
  }
  const [bank, setBank] = useState([]);
  const [msg, setMsg] = useState("");
  const navigate = useNavigate();

  const saveUser = async (e) => {
    e.preventDefault();
    try {
      await axios.post("http://localhost:5000/masterpembayaran", data);
      navigate("/payment");
    } catch (error) {
      if (error.response) {
        setMsg(error.response.data.msg);
      }
    }
  };

  useEffect(() => {
    const getBank = async () => {
      try {
        const response = await axios.get(`http://localhost:5000/bank`);
        const bytes =  cryptoJs.AES.decrypt(response.data, "ahmedhikendev2022");
        const decryptedData = JSON.parse(bytes.toString(cryptoJs.enc.Utf8));
        if(decryptedData){
          setBank(decryptedData);
        }
        else{
          setBank([])
        }
      } catch (error) {
        if (error.response) {
          setMsg(error.response.data.msg);
        }
      }
    };
    getBank();
  }, []);

  return (
    <div>
      <h1 className="title">Pembayaran</h1>
      <h2 className="subtitle">Add New Pembayaran</h2>
      <div className="card is-shadowless">
        <div className="card-content">
          <div className="content">
            <form onSubmit={saveUser}>
              <p className="has-text-centered">{msg}</p>
              <div className="field">
                <label className="label">Bank Name</label>
                <div className="control">
                  <select
                    className="input"
                    name="id_master_bank"
                    value={data.id_master_bank}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  >
                    <option value="">Choose bank</option>
                    {bank.map((item,key) => {
                      return(
                        <option key={key} value={item.uuid_master_bank}>{item.nama_master_bank}</option>
                      )
                    })}
                  </select>
                </div>
              </div>
              <div className="field">
                <label className="label">Name of payment</label>
                <div className="control">
                  <input
                    type="text"
                    className="input"
                    name="nama_master_pembayaran"
                    value={data.nama_master_pembayaran}
                    onChange={handleInputChange}
                    // placeholder="Name"
                  />
                </div>
              </div>

              <div className="field">
                <div className="control">
                  <button type="submit" className="button is-success">
                    Save
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAddBank;
