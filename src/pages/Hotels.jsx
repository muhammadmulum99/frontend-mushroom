import React from "react";
import Headeruser from "../components/headeruser/Headeruser";
import Contenthoteluser from "../components/contenthoteluser/Contenthoteluser";
import { Container } from "react-bootstrap";

const Hotels = () => {
  return (
    <>
      <Headeruser />
      <Contenthoteluser />
    </>
  );
};

export default Hotels;
