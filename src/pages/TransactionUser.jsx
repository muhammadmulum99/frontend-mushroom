import React from "react";
import Headeruser from "../components/headeruser/Headeruser";
import Contenttransaksi from "../components/contenttransaksi/Contenttransaksi";

function Transaksiuser() {
  return (
    <>
      <Headeruser />
      <Contenttransaksi />
    </>
  );
}

export default Transaksiuser;
