import React from "react";

import Contentpayment from "../components/contentpayment/Contentpayment";
import Headeruser from "../components/headeruser/Headeruser";

function Payment() {
  return (
    <>
      <Headeruser />
      <Contentpayment />
    </>
  );
}

export default Payment;
