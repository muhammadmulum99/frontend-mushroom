import React from "react";
import Headermanager from "../../components/header/headermanager/Headermanager";
import Sidebarmanager from "../../components/sidebar/sidebarmanagement/Sidebarmanager";
import Managementfacilities from "../../components/contentmanager/facilities/Managementfacilities";

const Facilitesmanager = () => {
  return (
    <>
      <div>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarmanager />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headermanager />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Managementfacilities />
        {/* [ Main Content ] start */}

        {/* [ Main Content ] end */}
        {/* [ Main Content ] end */}
      </div>
    </>
  );
};

export default Facilitesmanager;
