import React from "react";
import Headermanager from "../../components/header/headermanager/Headermanager";
import Sidebarmanager from "../../components/sidebar/sidebarmanagement/Sidebarmanager";
import Managementprofile from "../../components/contentmanager/profile/Managementprofile";

const Profilemanager = () => {
  return (
    <>
      <dev>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarmanager />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headermanager />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Managementprofile />
        {/* [ Main Content ] start */}

        {/* [ Main Content ] end */}
        {/* [ Main Content ] end */}
      </dev>
    </>
  );
};

export default Profilemanager;
