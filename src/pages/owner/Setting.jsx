import React from "react";
import Sidebarowner from "../../components/sidebar/sidebarowner/Sidebarowner";
import Headerowner from "../../components/header/headerowner/Headerowner";
import Settingcontent from "../../components/contentowner/setting/Settingcontent";

function Setting() {
  return (
    <>
      <dev>
        {/* [ Pre-loader ] start */}
        <div class="loader-bg">
          <div class="loader-track">
            <div class="loader-fill"></div>
          </div>
        </div>
        {/* [ Pre-loader ] End */}
        {/* [ navigation menu ] start */}
        <Sidebarowner />
        {/* [ navigation menu ] end */}

        {/* [ Header ] start */}
        <Headerowner />
        {/* [ Header ] end */}

        {/* [ Main Content ] start */}
        <Settingcontent />
        {/* [ Main Content ] end */}
      </dev>
    </>
  );
}

export default Setting;
