import React from "react";
import Headeruser from "../components/headeruser/Headeruser";
import Contentdetailhotel from "../components/contenthotel/Contentdetailhotel";
const Detailhotel = () => {
  return (
    <>
      <Headeruser />
      <Contentdetailhotel />
    </>
  );
};

export default Detailhotel;
