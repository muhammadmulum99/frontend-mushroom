import React from "react";
import Headeruser from "../components/headeruser/Headeruser";
import Footeruser from "../components/footeruser/Footeruser";
import Contentuser from "../components/contentuser/Contentuser";
import Contenttengahuser from "../components/contenttengahuser/Contenttengahuser";

const Homeuser = (props) => {
  return (
    <>
      <Headeruser />
      <Contentuser />
      <Contenttengahuser />
      {/* <Footeruser /> */}
    </>
  );
};

export default Homeuser;
