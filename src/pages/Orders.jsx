import React from "react";
import Headeruser from "../components/headeruser/Headeruser";
import Contentwishlistuser from "../components/contenthoteluser/Contentorderuser";
const Wishlist = () => {
  return (
    <>
      <Headeruser />
      <div style={{ marginBottom: "90px" }}>
        <Contentwishlistuser />
      </div>
      {/* <div>Wishlist</div> */}
    </>
  );
};

export default Wishlist;
